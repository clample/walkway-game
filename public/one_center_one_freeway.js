// Author: Maxime Schoemans

// Class implementing the solution to the 1-Center and 1-Freeway problem
class OneCenterOneFreewayProblem {
    
    // Solve the complete problem
    // If length is null, solve the variable-length problem
    // Else, solve the fixed-length problem
    static solve(points, v, length=null) {
        let pointset = new PointSet(points);
        if (length==null) {
            length = pointset.bboxDiameter();
        }
        let result = null;
        let best_R = Infinity;
        let rotate = [0, -90, -90, 0]
        let reflect = [false, true, false, true];
        let reflect_axis = [null, 1, null, 0]
        for (var i = 0; i < 4; i++) {
            let ps = pointset.transform(rotate[i], 1);
            if (reflect[i])
                ps = ps.reflect(reflect_axis[i]);
            let ch_points = ps.convexHullPoints();
            let new_full_result = this.solvePI_4(ch_points, v, length);
            if (new_full_result == null) {
                continue; // Sometimes LP solver fails, just ignore these cases.
            }
            let new_result = new_full_result.cf.transform(-rotate[i], 1);
            if (reflect[i])
                new_result = new_result.reflect();
            let new_R = new_full_result.R;
            if (new_R < best_R) {
                best_R = new_R;
                result = new_result;
            }
        }
        return {'cf': result, 'R': best_R};
    }

    // Solve the problem under the assumption 
    // that the highway angle is between 0 and PI/4
    // (angle between highway and x-axis)
    static solvePI_4(ch_points, v, length) {
        let n = ch_points.length;

        let result = null;
        let best_R = Infinity;

        let precision = 1000;
        for (var i = 0; i < precision; i++) { // Test for 1000 values of phi between 0 and PI/4
            let phi = (i/precision) * Math.PI / 4;
            let new_result = this.solvePhi(ch_points, phi, v, length);
            if (new_result == null) {
                continue; // Sometimes LP solver fails, just ignore these cases.
            }
            if (new_result.R < best_R) {
                best_R = new_result.R;
                result = new_result;
            }
        }
        // Testing only the values returned by the funcition below was insufficient
        // to always get the optimal result
        /*let phis = this.getPhis(ch_points, v);
        for (let phi of phis) {
            let new_result = this.solvePhi(ch_points, phi, v, length);
            if (new_result == null) {
                continue;
            }
            if (new_result.R < best_R) {
                best_R = new_result.R;
                result = new_result;
            }
        }*/
        return result;
    }

    // Get the values of phi at which the extreme set changes
    static getPhis(ch_points, v) {
        let n = ch_points.length;
        let points = [];
        for (var i = 0; i < ch_points.length; i++) {
            points.push(ch_points[i]);
        }
        points.push(ch_points[0]);

        let phi_min = 0;
        let phi_star = (Math.PI / 4) - Math.asin(Math.sqrt(2) / (2 * v));
        let phi_max = Math.PI / 4;

        let s_min = Math.sin(phi_min);
        let s_star = Math.sin(phi_star);
        let s_max = Math.sin(phi_max);

        let c_min = Math.cos(phi_min);
        let c_star = Math.cos(phi_star);
        let c_max = Math.cos(phi_max);

        let alpha_min = (1 - v * s_min) / (v * c_min);
        let alpha_max = (1 - v * s_max) / (v * c_max);

        let beta_min = (1 + v * s_min) / (v * c_min);
        let beta_star = (1 + v * s_star) / (v * c_star);

        let gamma_star = (1 - v * c_star) / (v * s_star);
        let gamma_max = (1 - v * c_max) / (v * s_max);

        let phis = [phi_min, phi_star, phi_max];
        for (var i = 0; i < n; i++) {
            let diff = Vector.sub(points[i], points[i + 1]);
            if (diff.x == 0)
                continue;
            let val = -diff.y / diff.x;
            console.log(val);
            if (alpha_max < val && val < alpha_min) {
                let phi = Math.atan2(1, val) - Math.acos(1 / (v * Math.sqrt(Math.pow(val, 2) + 1)));
                console.log("alpha " + phi / Math.PI);
                phis.push(phi);
            }
            if (-beta_star < val && val < -beta_min) {
                let phi = Math.atan2(-1, val) + Math.acos(1 / (v * Math.sqrt(Math.pow(val, 2) + 1)));
                console.log("beta " + phi / Math.PI);
                phis.push(phi);
            }
            if (1/gamma_max < val && val < 1/gamma_star) {
                let phi = Math.atan2(val, 1) + Math.acos(1 / (v * Math.sqrt(Math.pow(val, 2) + 1)));
                console.log("gamma " + phi / Math.PI);
                phis.push(phi);
            }
        }
        return phis;
    }

    // Solve the problem given an angle phi for the highway
    // Computes the extreme set for this angle and then calls the LP solver
    static solvePhi(ch_points, phi, v, length) {
        let phi_star = (Math.PI / 4) - Math.asin(Math.sqrt(2) / (2 * v));
        let lv = length / v;
        let s = Math.sin(phi);
        let c = Math.cos(phi);
        let max1 = - ch_points.reduce(function(a, b) {
            return Math.min(a, b.x + b.y);
        }, Infinity);
        let max2 = ch_points.reduce(function(a, b) {
            return Math.max(a, b.x + b.y);
        }, -Infinity);
        let alpha = (1 - v * s) / (v * c);
        let max3 = ch_points.reduce(function(a, b) {
            return Math.max(a, alpha * b.x + b.y);
        }, -Infinity);
        let max6 = - ch_points.reduce(function(a, b) {
            return Math.min(a, alpha * b.x + b.y);
        }, Infinity);
        let beta;
        let gamma;
        let max4;
        let max5;
        let max7;
        let max8;
        if (phi < phi_star) {
            max4 = ch_points.reduce(function(a, b) {
                return Math.max(a, b.x - b.y);
            }, -Infinity);
            max5 = - ch_points.reduce(function(a, b) {
                return Math.min(a, b.x - b.y);
            }, Infinity);
            beta = (1 + v * s) / (v * c);
            max7 = - ch_points.reduce(function(a, b) {
                return Math.min(a, beta * b.x - b.y);
            }, Infinity);
            max8 = ch_points.reduce(function(a, b) {
                return Math.max(a, beta * b.x - b.y);
            }, -Infinity);
        } else {
            gamma = (1 - v * c) / (v * s);
            max4 = ch_points.reduce(function(a, b) {
                return Math.max(a, b.x + gamma * b.y);
            }, -Infinity);
            max5 = - ch_points.reduce(function(a, b) {
                return Math.min(a, b.x + gamma * b.y);
            }, Infinity);
            max7 = - ch_points.reduce(function(a, b) {
                return Math.min(a, b.x - b.y);
            }, Infinity);
            max8 = ch_points.reduce(function(a, b) {
                return Math.max(a, b.x - b.y);
            }, -Infinity);
        }
        let objectiveFunction = [-1, 0, 0, 0]
        let inequalities = [
            { lhs: [1, -1, -1, -lv], rhs: max1 },
            { lhs: [1, 1, 1, lv], rhs: max2 + lv - length * (c + s) },
            { lhs: [1, alpha, 1, lv], rhs: max3 }
        ];
        if (phi < phi_star) {
            inequalities.push(...[
                { lhs: [1, 1, -1, lv], rhs: max4 + lv - length * (c - s) },
                { lhs: [1, -1, 1, -lv], rhs: max5 }
            ]);
        } else {
            inequalities.push(...[
                { lhs: [1, 1, gamma, lv], rhs: max4 },
                { lhs: [1, -1, -gamma, -lv], rhs: max5 }
            ]);
        }
        inequalities.push({ lhs: [1, -alpha, -1, -lv], rhs: max6 });
        if (phi < phi_star) {
            inequalities.push(...[
                { lhs: [1, -beta, 1, -lv], rhs: max7 },
                { lhs: [1, beta, -1, lv], rhs: max8 }
            ]);
        } else {
            inequalities.push(...[
                { lhs: [1, -1, 1, -length * (c - s)], rhs: max7 },
                { lhs: [1, 1, -1, length * (c - s)], rhs: max8 }
            ]);
        }
        let bounds = ch_points.reduce(function(a, b) {
            let max_b = Math.max(Math.abs(b.x), Math.abs(b.y));
            return Math.max(a, max_b);
        }, -Infinity);
        inequalities.push({ lhs: [0, 0, 0, 1], rhs: 0 });
        inequalities.push({ lhs: [0, 0, 0, -1], rhs: -1 });
        let lp_problem = new BoundedLinearProgram(inequalities, objectiveFunction, bounds);
        let solution = lp_problem.solve();
        if (solution == null) {
            /*console.log(phi);
            console.log(inequalities);
            console.log(objectiveFunction);*/
            return solution;
        }
        let R = solution[0]
        let x1 = solution[1]
        let y1 = solution[2]
        let theta = solution[3]
        let xf = x1 + length*theta*Math.cos(phi)
        let yf = y1 + length*theta*Math.sin(phi)
        let x2 = x1 + length*Math.cos(phi)
        let y2 = y1 + length*Math.sin(phi)
        return { 
            'cf': new OptCF(
                new Vector(xf, yf), 
                new Vector(x1, y1), 
                new Vector(x2, y2), 
                v), 
            'R': R
        };
    }
}