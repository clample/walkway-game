// Author: Maxime Schoemans

// utility functions used in the grahamScan

function sign(x) {
    if (x > 0) {
        return 1;
    } else if (x < 0) {
        return -1;
    } else {
        return 0;
    }
}

function detVal(r,p,q) {
    pq = Vector.sub(p,q);
    pr = Vector.sub(p,r);
    det = pq.x*pr.y - pq.y*pr.x;
    return det;
}

function detSign(r,p,q) {
    det = detVal(r,p,q);
    return sign(det);
}