// Authors: Maxime Schoemans and Chris Lample

class BoundedLinearProgram {

    constructor(inequalities, objectiveFunction, bound) {
        this._inequalities = inequalities;
        this._objectiveFunction = objectiveFunction;
        this._bound = bound;
    }

    get inequalities() {
        return this._inequalities;
    }

    get objectiveFunction() {
        return this._objectiveFunction;
    }

    get bound() {
        return this._bound;
    }
    
    // Solve a system of linear equations
    // inequalities should have the form {lhs: [], rhs: some number} representing "c1 * x1 + ... + cn * xn >= rhs"
    // objective function should be an array, representing the vector <v1, ..., vn>
    // We will try to maximize the objective function, subject to the inequalities
    // Also pass a bound (i.e. 1000). This will ensure none of the variables are greater than 1000 or less than -1000.
    solve() {
        if (this.objectiveFunction.length == 1) {
            let result = this.solveOneDimensionalLP();
            /*if (result == null) {
                console.log(this.inequalities);
                console.log(this.objectiveFunction);
            }*/
            return result;
        }
        
        let v = this.getInitialValue();
        let previousInequalities = [];
        for (let inequality of this.inequalities) {
            if (!this.satisfiesInequality(v, inequality)) {
                v = this.getNewValue(inequality, previousInequalities);
                if (v == null) {                    
                    return v;
                }
            }
            
            previousInequalities.push(inequality);
        }
        
        return v;
    }

    solveOneDimensionalLP() {
        /*console.log(this.inequalities);*/
        let upperBounds = [this.bound];
        let lowerBounds = [-this.bound];
        for (let inequality of this.inequalities) {
            let a = inequality.lhs[0];
            if (a == 0 && inequality.rhs > 0) {
                // We have inequality of the form "0 > some number"
                // This is a contradiction and can't be satisfied
                /*console.log("contradiction");*/
                return null;
            } else if (a == 0) {
                continue;
            }

            let bound = inequality.rhs / a;
            if (a > 0) {
                lowerBounds.push(bound);
            } else if (a < 0) {
                upperBounds.push(bound);
            }
        }

        let upperBound = Math.min(...upperBounds);
        let lowerBound = Math.max(...lowerBounds);

        if (lowerBound > upperBound) {
            /*console.log("bounds")*/
            return null;
        }
        
        if (this.objectiveFunction[0] > 0) {
            return [upperBound];
        } else {
            return [lowerBound];
        }
    }
    
    getInitialValue() {
        // objectiveFunction is a vector originating from the origin.
        // Parameterising the vector by t gives a line.
        // We can tell which bound the vector will intersect first by looking at the max component.
        // We can then solve for the paremeter t, and return the value of the line at t.
        let maxComponent = Math.max(
            ...this.objectiveFunction.map(v => Math.abs(v))
        );
        
        let t = this.bound / maxComponent;

        return this.objectiveFunction.map(v => v * t);
    }

    satisfiesInequality(value, inequality) {
        if (value.length != inequality.lhs.length) {
            throw "Incompatible value and inequality";
        }
        let lhs = 0;
        for (let i = 0; i < value.length; i++) {
            lhs += value[i] * inequality.lhs[i];
        }

        return lhs >= inequality.rhs;
    }

    // Return a new solution to the system of inequalities
    // "hyperplane" is the most recent inequality
    // inequalities is the set of previously added inequalities
    getNewValue(hyperplane, inequalities) {

        // We know that the solution is on the hyperplane c1*x1 +... + cn*xn = rhs
        // We solve for xn, and plug the value into the inequalities a1*x1 + ... + an *xn = rhs
        // This gives us a system of inequalities in d-1 dimensions
        // For the objective function, we project it onto the hyperplane
        // We then have a LP problem in d-1 dimensions


        let index = null;
        for (var i = hyperplane.lhs.length - 1; i >= 0; i--) {
            if (hyperplane.lhs[i] != 0) {
                index = i;
                break;
            }
        }
        if (index == null) {
            throw "No nonzero coeffs in inequality";
        }
        let cn = hyperplane.lhs[index];

        let reducedInequalities = inequalities.map(inequality => {
            let an = inequality.lhs[index];
            let newLHS = [];
            for (let i = 0; i < inequality.lhs.length; i++) {
                if (i != index)
                    newLHS.push(inequality.lhs[i] - (an * hyperplane.lhs[i] / cn));
            }
            let newRHS = inequality.rhs - (an * hyperplane.rhs / cn);
            return {
                lhs: newLHS,
                rhs: newRHS
            }
        });

        let newObjectiveFunction = [];
        for (var i = 0; i < this.objectiveFunction.length; i++) {
            if (i != index)
                newObjectiveFunction.push(this.objectiveFunction[i]);
        }
        if (!newObjectiveFunction.reduce((acc, e) => acc || e != 0, false)) {
            // In case the objective function is all zeroes, all points are equally OK
            // We should set an arbitrary objective function
            newObjectiveFunction = Array(newObjectiveFunction.length).fill(1);
        }
        let newLinearProgram = new BoundedLinearProgram(reducedInequalities, newObjectiveFunction, this.bound);
        let val = newLinearProgram.solve();
        
        if (val == null) {
            return null;
        }

        // Val contains the optimal values for x1,...,x_n-1
        // We can now solve for xn, which we previously removed
        let numerator = hyperplane.rhs;
        let count = 0;
        for (var i = 0; i < hyperplane.lhs.length; i++) {
            if (i == index) {
                continue;
            }
            numerator -= hyperplane.lhs[i] * val[count];
            count += 1;
        }
        let xn = numerator / cn;
        val.splice(index, 0, xn);
        return val;
    }

    evaluateObjectiveFunction(value) {
        return BoundedLinearProgram.evaluateValue(value, this.objectiveFunction);
    }

    static testInequality(value, inequality) {
        return BoundedLinearProgram.evaluateValue(value, inequality.lhs) >= inequality.rhs;
    }

    // Check the value against the lhs / objective function
    static evaluateValue(value, lhs) {
        if (value.length != lhs.length) {
            throw "Cannot evaluate value with wrong length";
        }
        let sum = 0;
        for (let i = 0; i < value.length; i++) {
            sum += value[i] * lhs[i];
        }
        return sum;
    }

}
