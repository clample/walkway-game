// Author: Maxime Schoemans

// PointSet class
class PointSet {

    constructor(points) {
        this.points = points
    }

    get length() {
        return this.points.length
    }

    // Returns half of the width of the smallest square bounding box 
    // centered around (0, 0) that contains all the points.
    bboxDiameter() {
        let xs = this.sortedX(false);
        let ys = this.sortedY(false);
        let bottomLeft = new Vector(xs[0], ys[0]);
        let topRight = new Vector(xs[xs.length-1], ys[ys.length-1]);
        return bottomLeft.distance(topRight);
    }

    // Returns the sorted x-coordinates
    sortedX(reverse=false) {
        let valueSet = new Set();
        for (var i = 0; i < this.points.length; i++) {
            valueSet.add(this.points[i].x);
        }
        let values = [...valueSet];
        if (reverse) {
            values.sort(function(a, b){return b-a});
        } else {
            values.sort(function(a, b){return a-b});
        }
        return values
    }

    // Returns the sorted x-coordinates
    sortedY(reverse=true) {
        let valueSet = new Set();
        for (var i = 0; i < this.points.length; i++) {
            valueSet.add(this.points[i].y);
        }
        let values = [...valueSet];
        if (reverse) {
            values.sort(function(a, b){return b-a});
        } else {
            values.sort(function(a, b){return a-b});
        }
        return values
    }

    copy() {
        let points = []
        for (var i = 0; i < this.points.length; i++) {
            points.push(this.points[i].copy());
        }
        return new PointSet(points);
    }

    transform(rotate, scale) {
        let points = []
        for (var i = 0; i < this.points.length; i++) {
            points.push(this.points[i].transform(rotate, scale));
        }
        return new PointSet(points);
    }

    reflect(axis=0) {
        let points = []
        for (var i = 0; i < this.points.length; i++) {
            points.push(this.points[i].reflect(axis));
        }
        return new PointSet(points);
    }

    // Returns the extreme set/smallest bounding box containing the points.
    extremeSet() {
        let sortedX = this.sortedX(false);
        let sortedY = this.sortedY(false);
        let minX = sortedX[0];
        let maxX = sortedX[sortedX.length-1];
        let minY = sortedY[0];
        let maxY = sortedY[sortedY.length-1];
        return new ExtremeSet(minX, maxX, minY, maxY);
    }

    subSet(start, end) {
        let points = [];
        for (var i = 0; i < this.points.length; i++) {
            if (i >= start && i < end)
                points.push(this.points[i].copy());
        }
        return new PointSet(points);
    }

    // GrahamScan
    convexHullPoints() {
        return grahamScan(this.points);
    }
}

function grahamScan(points) {
    let fid = 0;
    let pids = [];
    let ch_points = [];
    let nch_points = [];
    for (var i = points.length - 1; i >= 1; i--) {
        if (points[i].y > points[fid].y) {
            fid = i;
        } else if (points[i].y == points[fid].y && points[i].x > points[fid].x) {
            fid = i;
        }
    }
    for (var i = points.length - 1; i >= 0; i--) {
        if (i != fid) {
            pids.push(i);
        }
    }
    ch_points.push(fid);
    pids.sort(function(a,b){
        pivot = points[fid];
        p = points[a];
        q = points[b];
        pip = Vector.sub(pivot,p).normalize();
        piq = Vector.sub(pivot,q).normalize();
        return pip.x - piq.x;
    });
    pids.push(fid);
    ch_points.push(pids.splice(0,1)[0]);
    let len = pids.length;
    for (var i = 0; i < len; i++) {
        ch_points.push(pids.splice(0,1)[0]);
        while (ch_points.length > 2) {
            if (detSign(
                    points[ch_points[ch_points.length-3]],
                    points[ch_points[ch_points.length-2]],
                    points[ch_points[ch_points.length-1]]
                ) >= 0) {
                nch_points.push(ch_points.splice(ch_points.length-2,1)[0]);
            } else {
                break;
            }
        }
    }
    ch_points.splice(ch_points.length-1,1);
    new_points = [];
    for (var i = 0; i < ch_points.length; i++) {
        new_points.push(points[ch_points[i]].copy());
    }
    return new_points;
}