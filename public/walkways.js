// Author: Chris Lample

let requestedOneDOptimal = false;
let requestedClearOneD = false;

function requestOneDOptimal() {
    requestedOneDOptimal = true;
}

function clearOneD() {
    requestedClearOneD = true;
}

let requestedTwoDOptimal = false;
let requestedClearTwoD = false;

function requestTwoDOptimal() {
    requestedTwoDOptimal = true;
}

function clearTwoD() {
    requestedClearTwoD = true;
}

let oneDWalkway = (sketch) => {
    let width = 700;
    let height = 300;
    let v = 2;

    let selectedPoint;
    let points = [ new Vector(100, height / 2), new Vector(400, height / 2) ];
    let walkway = {
        a: new Vector(200, height/2),
        b: new Vector(300, height/2)
    };
    let optimalWalkway;

    sketch.setup = () => {
        let canvas = sketch.createCanvas(width, height);
        canvas.parent("one-d-walkway-canvas");
        updateTravelTimeDiameter();
    }

    sketch.draw = () => {
        if (requestedClearOneD) {
            requestedClearOneD = false;
            resetState();
            updateTravelTimeDiameter();
        }
        
        sketch.background(255);
        sketch.fill("black");
        sketch.stroke("black");
        for (let point of points) {
            sketch.ellipse(point.x,point.y,10,10);
        }
        drawWalkway(walkway, "blue");

        if (requestedOneDOptimal) {
            requestedOneDOptimal = false;
            updateOptimalWalkway();
        }
        if (optimalWalkway != null) {
            drawWalkway(optimalWalkway, "red");
        }
    }

    sketch.mousePressed = () => {
        let mousePos = new Vector(sketch.mouseX, sketch.mouseY);
        for (let point of points) {
            if (mousePos.distance(point) < 10) {
                selectedPoint = point;
                return;
            }
        }
        if (mousePos.distance(walkway.a) < 10) {
            selectedPoint = walkway.a;
            return;
        } else if (mousePos.distance(walkway.b) < 10) {
            selectedPoint = walkway.b;
        }
    }

    sketch.mouseDragged = () => {
        if (selectedPoint != null) {
            let x = Math.min(width-10, Math.max(10, sketch.mouseX))
            selectedPoint.moveTo(x, height/2);
            updateTravelTimeDiameter();
        }
    }

    sketch.mouseReleased = () => {
        selectedPoint = null;
    }

    sketch.doubleClicked = () => {
        points.push(new Vector(sketch.mouseX, height/2));
        updateTravelTimeDiameter();
    }

    let drawWalkway = function(w, color="black") {
        sketch.fill(color);
        sketch.stroke(color);
        sketch.line(w.a.x, w.a.y, w.b.x, w.b.y);
        sketch.ellipse(w.a.x, w.a.y, 10, 10);
        sketch.ellipse(w.b.x, w.b.y, 10, 10);
        sketch.fill("black");
        sketch.stroke("black");
    }

    let updateTravelTimeDiameter = function() {
        let walkwayPoints = points.map(p => p.x);
        let walkwayProb = new WalkwayProblem1D(walkwayPoints, v);
        let userWalkway = { a: walkway.a.x, b: walkway.b.x };

        document.getElementById('one-d-travel-diameter').innerHTML = "Travel time diameter: " + walkwayProb.travelTimeDiameter(userWalkway).d;
    }

    let updateOptimalWalkway = function() {
        let walkwayPoints = points.map(p => p.x);
        let walkwayProb = new WalkwayProblem1D(walkwayPoints, v);
        let optimal = walkwayProb.optimalWalkwayLocation();
        optimalWalkway = {
            a: new Vector(optimal.a, height / 2 - 20),
            b: new Vector(optimal.b , height / 2 - 20)
        }
    }

    let resetState = function() {
        points = [ new Vector(100, height / 2), new Vector(400, height / 2) ];
        walkway = {
            a: new Vector(200, height/2),
            b: new Vector(300, height/2)
        };
        optimalWalkway = null;
    }
};
let oneDWalkwaySketch = new p5(oneDWalkway);


let twoDWalkway = (sketch) => {
    let width = 700;
    let height = 300;
    let v = 2;

    let selectedPoint;
    let points = [new Vector(100, height / 2), new Vector(400, height / 2)];
    let walkway = {
        a: new Vector(200, height/2),
        b: new Vector(300, height/2)
    };
    let optimalWalkway;
    
    sketch.setup = () => {
        let canvas = sketch.createCanvas(width, height);
        canvas.parent("two-d-walkway-canvas");
        updateTravelTimeDiameter();
    }

        sketch.draw = () => {
        if (requestedClearTwoD) {
            requestedClearTwoD = false;
            resetState();
            updateTravelTimeDiameter();
        }
        
        sketch.background(255);
        sketch.fill("black");
        sketch.stroke("black");
        for (let point of points) {
            sketch.ellipse(point.x,point.y,10,10);
        }
        drawWalkway(walkway, "blue");

        if (requestedTwoDOptimal) {
            requestedTwoDOptimal = false;
            updateOptimalWalkway();
        }
        if (optimalWalkway != null) {
            drawWalkway(optimalWalkway, "red");
        }
    }

    sketch.mousePressed = () => {
        let mousePos = new Vector(sketch.mouseX, sketch.mouseY);
        for (let point of points) {
            if (mousePos.distance(point) < 10) {
                selectedPoint = point;
                return;
            }
        }
        if (mousePos.distance(walkway.a) < 10) {
            selectedPoint = walkway.a;
            return;
        } else if (mousePos.distance(walkway.b) < 10) {
            selectedPoint = walkway.b;
        }
    }

    sketch.mouseDragged = () => {
        if (selectedPoint != null) {
            let x = Math.min(width-10, Math.max(10, sketch.mouseX));
            let y = Math.min(height-10, Math.max(10, sketch.mouseY));
            selectedPoint.moveTo(x, y);
            updateTravelTimeDiameter();
        }
    }

    sketch.mouseReleased = () => {
        selectedPoint = null;
    }

    sketch.doubleClicked = () => {
        points.push(new Vector(sketch.mouseX, sketch.mouseY));
        updateTravelTimeDiameter();
    }

    let drawWalkway = function(w, color="black") {
        sketch.fill(color);
        sketch.stroke(color);
        sketch.line(w.a.x, w.a.y, w.b.x, w.b.y);
        sketch.ellipse(w.a.x, w.a.y, 10, 10);
        sketch.ellipse(w.b.x, w.b.y, 10, 10);
        sketch.fill("black");
        sketch.stroke("black");
    }

    let updateTravelTimeDiameter = function() {
        let walkwayPoints = points.map(p => new Vector(p.x, p.y));
        let walkwayProb = new WalkwayProblem2D(walkwayPoints, v, width, height);
        let userWalkway = {
            a: new Vector(walkway.a.x, walkway.a.y),
            b: new Vector(walkway.b.x, walkway.b.y)
        };

        document.getElementById('two-d-travel-diameter').innerHTML = "Travel time diameter: " + walkwayProb.getTravelTimeDiameter(userWalkway).toFixed(1);
    }

    let updateOptimalWalkway = function() {
        let walkwayPoints = points.map(p => new Vector(p.x, p.y));
        let walkwayProb = new WalkwayProblem2D(walkwayPoints, v, width, height);
        optimalWalkway = walkwayProb.optimalWalkwayLocation();
    }

    let resetState = function() {
        points = [ new Vector(100, height / 2), new Vector(400, height / 2) ];
        walkway = {
            a: new Vector(200, height/2),
            b: new Vector(300, height/2)
        };
        optimalWalkway = null;
    }

};

let twoDWalkwaySketch = new p5(twoDWalkway);
