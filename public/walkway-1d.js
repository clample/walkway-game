// Author: Chris Lample

class WalkwayProblem1D {

    // assume at least two points
    constructor(points, v) {
        points.sort((p1, p2) => p1 - p2);
        this._points = points;
        this._v = v;
    }

    get points() {
        return this._points;
    }

    get v() {
        return this._v;
    }

    optimalWalkwayLocation() {
        // We walk the array several times, but we could merge some of these
        let maxPoint = Math.max(...this.points);
        let minPoint = Math.min(...this.points);
        let scaledPoints = this.points.map(p => (p - minPoint) / (maxPoint - minPoint));
        let r1 = Math.max(...scaledPoints.filter(
            point => point <= 1 - (this.v / (2 * this.v - 1))
        ));
        let s1 = Math.min(...scaledPoints.filter(
            point => point >= (r1 * (this.v - 1) + this.v + 1)/(3 * this.v - 1)
        ));
        let s2 = Math.min(...scaledPoints.filter(
            point => point >= this.v / (2 * this.v - 1)
        ));
        let r2 = Math.max(...scaledPoints.filter(
            point => point <= ((this.v - 1) * (s2 + 1))/(3 * this.v -1)
        ));

        let walkway1 = {'a': r1 / 2, 'b': (s1 + 1) / 2};
        let distance1 = this.travelTimeDiameterOptimalWalkway(scaledPoints, walkway1, r1, s1);

        let walkway2 = {'a': r2 / 2, 'b': (s2 + 1) / 2};
        let distance2 = this.travelTimeDiameterOptimalWalkway(scaledPoints, walkway2, r2, s2);

        let unscale = p => p * (maxPoint - minPoint) + minPoint;
        if (distance1 <= distance2) {
            return {
                'a': unscale(walkway1.a),
                'b': unscale(walkway1.b)
            }
        } else {
            return {
                'a': unscale(walkway2.a),
                'b': unscale(walkway2.b)
            }
        }
    }

    // Points should be scaled to [0,1]
    travelTimeDiameterOptimalWalkway(points, walkway, r, s) {
        let pointsInRS = points.filter(p => p > r && p < s);
        let bothInRS = Math.max(...pointsInRS) - Math.min(...pointsInRS);
        return Math.max(
            this.travelTimeDistance(0, 1, walkway),
            this.travelTimeDistance(0, s, walkway),
            this.travelTimeDistance(r, 1, walkway),
            this.travelTimeDistance(r, s, walkway),
            bothInRS,
            r,
            1 - s
        );
    }

    travelTimeDiameter(walkway) {
        walkway = this.normalizeWalkway(walkway);
        
        let typeIandIV = this.travelTime(walkway, 0, this.points.length - 1);
        let sameHalf = this.maxDistanceSameHalf(walkway);
        let typeIIandIII = this.maxDistanceTypeIIandIII(walkway)
        let typeIandIII = this.maxDistanceIandIII(walkway);
        let typeIIandIV = this.maxDistanceIIandIV(walkway);

        return [typeIandIV, sameHalf, typeIandIII, typeIIandIV]
            .reduce((max, newVal) => {
                if (max == null) {
                    return newVal;
                } else if (newVal == null) {
                    return max;
                }

                return newVal.d > max.d ? newVal : max;
            });
    }

    normalizeWalkway(walkway) {
        if (walkway.a <= walkway.b) {
            return walkway;
        }

        let temp = walkway.a;
        walkway.a = walkway.b;
        walkway.b = temp;
        return walkway;
    }

    travelTimeDiameterBruteForce(walkway) {
        let maxDistance = null;
        for (let i=0; i < this.points.length - 1; i++) {
            for (let j= i + 1; j < this.points.length; j++) {
                let distance = this.travelTime(walkway, i, j);
                if (maxDistance == null || distance.d > maxDistance.d) {
                    maxDistance = distance;
                }
            }
        }
        return maxDistance;
    }

    // May return null if points no two points are in the same half
    maxDistanceSameHalf(walkway) {
        let halfwayIndex = this.points
            .findIndex(p => p > walkway.a + walkway.b / 2);

        if (halfwayIndex == -1 || halfwayIndex == 0) {
            // All points are in the same half
            return this.travelTime(walkway, 0, this.points.length - 1);
        }
        let firstHalf = this.points.slice(0, halfwayIndex);
        let secondHalf = this.points.slice(halfwayIndex);
        if (firstHalf.length == 1 && secondHalf.length == 1) {
            return null;
        } else if (firstHalf.length == 1) {
            return this.travelTime(walkway, 1, this.points.length - 1);
        } else if (secondHalf.length == 1) {
            return this.travelTime(walkway, 0, this.points.length - 1);
        } else {
            let dHalfOne = this.travelTime(walkway, 0, halfwayIndex - 1);
            let dHalfTwo = this.travelTime(walkway, halfwayIndex, this.points.length - 1);
            return dHalfOne.d >= dHalfTwo ? dHalfOne : dHalfTwo;
        }

    }

    maxDistanceTypeIIandIII(walkway) {
        let relevantPoints = this.points
            .filter(point => point > walkway.a && point <= walkway.b);
        let offset = this.points.findIndex(p => p > walkway.a);
        if (relevantPoints.length < 2) {
            return null;
        } else if (relevantPoints.length == 2) {
            // There are not enough points to compute the convex hull,
            // so we simply calculate the travel time
            return this.travelTime(walkway, offset, offset + 1);
        }

        let convexHull = this.circularRepresentation(relevantPoints, walkway);
        return convexHull.antipodalVertices().map(
            pair => this.travelTime(walkway, offset + pair[0], offset + pair[1])                                       
        ).reduce((max, newVal) => {
            return newVal.d > max.d ? newVal : max;
        });
    }

    // Brute force implementation used for testing
    maxDistanceTypeIIandIIIBruteForce(walkway) {
        let relevantPoints = this.points
            .filter(point => point > walkway.a && point <= walkway.b);
        let offset = this.points.findIndex(p => p > walkway.a);
        let maxDistance = null;
        for (let i = 0; i < relevantPoints.length - 1; i++) {
            for (let j = i + 1; j < relevantPoints.length; j++) {
                let distance = this.travelTime(walkway, offset + i, offset + j);
                if (maxDistance == null || distance.d > maxDistance.d) {
                    maxDistance = distance;
                }
            }
        }
        return maxDistance;
    }

    maxDistanceIIandIV(walkway) {
        let halfwayPoint = walkway.a + walkway.b / 2;
        let sectionIIPoints = this.points
            .filter(point => point > walkway.a && point <= halfwayPoint);
        let sectionIIOffset = this.points.findIndex(p => p > walkway.a);
        let sectionIVPoints = this.points
            .filter(point => point > walkway.b);
        if (sectionIIPoints.length == 0 || sectionIVPoints.length == 0) {
            return null;
        } else if (sectionIIPoints.length == 1) {
            return this.travelTime(walkway, sectionIIOffset, this.points.length - 1);
        } else if (sectionIIPoints.length == 2) {
            let option1 = this.travelTime(walkway, sectionIIOffset, this.points.length - 1);
            let option2 = this.travelTime(walkway, sectionIIOffset + 1, this.points.length - 1);
            return option1.d >= option2.d ? option1 : option2;
        }

        let convexHull = this.circularRepresentation(sectionIIPoints, walkway);
        let pointB = this.circularRepresentationOfPoint(walkway.b, walkway);
        let furthestPoint = convexHull.furthestPointFrom(pointB);
        return this.travelTime(walkway, sectionIIOffset + furthestPoint, this.points.length - 1);
    }


    maxDistanceIandIII(walkway) {
        let halfwayPoint = (walkway.a + walkway.b) / 2;
        let sectionIPoints = this.points
            .filter(point => point <= walkway.a);
        let sectionIIIPoints = this.points
            .filter(point => point > halfwayPoint && point <= walkway.b);
        let sectionIIIOffset = this.points.findIndex(point => point > halfwayPoint);
        if (sectionIPoints.length == 0 || sectionIIIPoints.length == 0) {
            return null;
        } else if (sectionIIIPoints.length == 1) {
            return this.travelTime(walkway, 0, sectionIIIOffset);
        } else if (sectionIIIPoints.length == 2) {
            let option1 = this.travelTime(walkway, 0, sectionIIIOffset);
            let option2 = this.travelTime(walkway, 0, sectionIIIOffset + 1);
            return option1.d >= option2.d ? option1 : option2;
        }

        let convexHull = this.circularRepresentation(sectionIIIPoints, walkway);
        let pointA = this.circularRepresentationOfPoint(walkway.a, walkway);
        let furthestPoint = convexHull.furthestPointFrom(pointA);
        return this.travelTime(walkway, 0, sectionIIIOffset + furthestPoint);
    }


    // Produce a convex hull by mapping points to the unit circle
    // Points should all be from region II and III
    circularRepresentation(points, walkway) {
        if (points.length < 3) {
            throw "Unable to produce circular represenation with less than 3 points";
        }

        let convexHullPoints = points.map(
            point => this.circularRepresentationOfPoint(point, walkway)
        );

        return new ConvexHull(convexHullPoints);    
    }

    circularRepresentationOfPoint(point, walkway) {
        let totalDistance = (walkway.b - walkway.a) + ((walkway.b - walkway.a) / this.v);
        let theta = 2 * Math.PI * (point - walkway.a) / totalDistance;
        return {
            'x': Math.cos(theta),
            'y': Math.sin(theta)
        };
    }

    travelTime(walkway, i, j) {
        return {
            'p1': i,
            'p2': j,
            'd': this.travelTimeDistance(this.points[i], this.points[j], walkway)
        };
    }

    travelTimeDistance(point1, point2, walkway) {
        return Math.min(
            Math.abs(point1 - point2),
            Math.abs(point2 - walkway.a) +
                (Math.abs(walkway.b - walkway.a) / this.v) +
                Math.abs(point1 - walkway.b),
            Math.abs(point2 - walkway.b) +
                (Math.abs(walkway.b - walkway.a) / this.v) +
                Math.abs(point1 - walkway.a)
        );
    }
}
