// Authors: Maxime Schoemans and Chris Lample

let width = 800;
let height = 400;

let margin = 15;
let minDist = 15;

let v = 2;
let num_points = 20;

let gameScenario = null;

let points = null;
let selectedPoint = null;

let highway = null;
let optimalHighway = null;

function setup() {
    let canvas = createCanvas(width, height);
    canvas.parent('game-canvas');
    
}

function draw() {
    background(255);
    if (points != null)
        for (let point of points)
            drawPoint(point);
    if (highway != null)
        highway.draw();
    if (optimalHighway != null)
        optimalHighway.draw("red");
}

function drawPoint(p) {
    ellipse(p.x,p.y,15,15);
}

function drawLine(p1, p2) {
    line(p1.x, p1.y, p2.x, p2.y);
}

function mousePressed() {
    let mousePos = new Vector(mouseX, mouseY);
    if (highway != null)
        selectedPoint = highway.getClosestPoint(mousePos);
}

function mouseDragged() {
    if (selectedPoint != null) {
        let x = Math.min(width-15, Math.max(15, mouseX))
        let y = Math.min(height-15, Math.max(15, mouseY))
        if (gameScenario == '1-d walkway')
            selectedPoint.moveTo(x, height/2);
        else
            selectedPoint.moveTo(x, y);
        highway.update();
    }
}

function mouseReleased() {
    selectedPoint = null;
}

function oneDWalkwayClicked() {
    hideHomeButtons();
    showGameButtons();
    gameScenario = '1-d walkway';
    let n = Math.floor(num_points / 2);
    points = generatePointset(n, true);
    let a = new Vector(width/4, height/2);
    let b = new Vector(3*width/4, height/2);
    highway = new Walkway(a, b, v);
    optimalHighway = null;
}

function twoDWalkwayClicked() {
    hideHomeButtons();
    showGameButtons();
    gameScenario = '2-d walkway';
    points = generatePointset(num_points);
    let a = new Vector(width/4, height/4);
    let b = new Vector(3*width/4, 3*height/4);
    highway = new Walkway(a, b, v);
    optimalHighway = null;
}

function oneCenterOneTurnpike() {
    hideHomeButtons();
    showGameButtons();
    gameScenario = 'turnpike';
    points = generatePointset(num_points);
    let f = new Vector(3*width/4, height/4);
    let t1 = new Vector(width/4, height/4);
    let t2 = new Vector(3*width/4, 3*height/4);
    highway = new CT(f, t1, t2, v);
    optimalHighway = null;
}

function oneCenterOneFreeway() {
    hideHomeButtons();
    showGameButtons();
    gameScenario = 'freeway';
    points = generatePointset(num_points);
    let f = new Vector(3*width/4, height/4);
    let t1 = new Vector(width/4, height/4);
    let t2 = new Vector(3*width/4, 3*height/4);
    highway = new CF(f, t1, t2, v);
    optimalHighway = null;
}

function submitSolution() {
    let manualDist;
    let optimalDist;
    if (gameScenario == '1-d walkway') {
        let walkwayPoints = points.map(p => p.x);
        let walkwayProb = new WalkwayProblem1D(walkwayPoints, v);
        let optimal = walkwayProb.optimalWalkwayLocation();
        optimalHighway = new Walkway(
            new Vector(optimal.a, height / 2),
            new Vector(optimal.b, height / 2)
        );

        let userWalkway = { a: highway.a.x, b: highway.b.x };
        manualDist = walkwayProb.travelTimeDiameterBruteForce(userWalkway).d;
        optimalDist = walkwayProb.travelTimeDiameterBruteForce(optimal).d;
    } else if (gameScenario == '2-d walkway') {
        let walkwayProb = new WalkwayProblem2D(points, v, width, height);
        let optimal = walkwayProb.optimalWalkwayLocation();
        optimalHighway = new Walkway(optimal.a, optimal.b);

        manualDist = walkwayProb.getTravelTimeDiameter(highway);
        optimalDist = walkwayProb.getTravelTimeDiameter(optimalHighway);
    } else if (gameScenario == 'turnpike') {
        let result = OneCenterOneTurnpikeProblem.solve(points, v, highway.l);
        optimalHighway = result.ct;

        let ctDist = -Infinity;
        for (let point of points) {
            ctDist = Math.max(ctDist, highway.l1_dist(point));
        }
        manualDist = ctDist;
        optimalDist = result.R;
        
    } else if (gameScenario == 'freeway') {
        let result = OneCenterOneFreewayProblem.solve(points, v, highway.l);
        optimalHighway = result.cf;

        ctDist = -Infinity;
        for (let point of points) {
            ctDist = Math.max(ctDist, highway.l1_dist(point));
        }
        manualDist = ctDist;
        optimalDist = result.R;        
    }
    console.log(manualDist);
    console.log(optimalDist);
    let perc = Math.tanh((manualDist - optimalDist) / optimalDist);
    let score = (1 - perc)*100;
    score = score.toFixed(2);
    console.log("perc: " + perc);
    if (perc <= 0.1) {
        writeResults("Victory! (score = " + score + "%)");
    } else {
        writeResults("Defeat! (score = " + score + "%)");
    }
}

function generatePointset(n, oneD=false) {
    let points = [];
    count = 0;
    while (points.length < n) {
        count += 1;
        let newPoint;
        if (oneD)
            newPoint = new Vector(getRandomInt(margin, width-margin), height/2);
        else
            newPoint = new Vector(getRandomInt(margin, width-margin), getRandomInt(margin, height-margin));
        let add = true;
        for (var j = points.length - 1; j >= 0; j--) {
            if (newPoint.distance(points[j]) < minDist) {
                add = false;
                break;
            }
        }
        if (add) {
            points.push(newPoint);
            count = 0;
        }
        if (count == 100)
            break;
    }
    return points;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function hideHomeButtons() {
    document.getElementById('home-buttons').style.visibility = 'hidden';
}

function showHomeButtons() {
    document.getElementById('home-buttons').style.visibility = 'visible';
}

function hideGameButtons() {
    document.getElementById('game-buttons').style.visibility = 'hidden';
}

function showGameButtons() {
    document.getElementById('game-buttons').style.visibility = 'visible';
}

function writeResults(text) {
    document.getElementById('results').innerHTML = text;;
}

function clearResults() {
    document.getElementById('results').innerHTML = "";
}

function goHome() {
    hideGameButtons();
    showHomeButtons();
    clearResults();
    points = null;
    selectedPoint = null;
    highway = null;
    optimalHighway = null;
}
