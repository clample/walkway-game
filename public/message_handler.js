// Author: Maxime


// Messagehandler class
// Previsouly used to print info and error messages on the p5 canvas easily
// Not used in the latest version of the game
class MessageHandler {
    constructor() {
        this._showInfo = false;
        this._infoMessage = "";
        this._showError = false;
        this._errorMessage = "";
    }

    get infoMessage() {return this._infoMessage;}
    set infoMessage(m) {
        this._showInfo = true;
        this._infoMessage = m;
    }

    get errorMessage() {return this._errorMessage;}
    set errorMessage(m) {
        this._showError = true;
        this._errorMessage = m;
    }

    hideInfo() {this._showInfo = false;}
    hideError() {this._showError = false;}

    hide() {
        this.hideInfo();
        this.hideError();
    }

    report() {
        let posX = 0;
        let posY = 10;
        if (this._showInfo) {
            fill('green');
            text(this._infoMessage, posX, posY);
            posY += 10;
        }
        if (this._showError) {
            fill('red');
            text(this._errorMessage, posX, posY);
        }
        fill('black');
    }
}