// Author: Maxime Schoemans

// Class implementing the solution to the 1-Center problem
class OneCenterProblem {

    // Solves the 1-Center problem using linfinity distance
    static solve(pointset) {
        let es = pointset.extremeSet();
        let c = es.centerPoint();
        let R = es.delta;
        return {'c': c, 'R': R};
    }
}