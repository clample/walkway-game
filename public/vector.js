// Authors: Maxime Schoemans and Chris Lample

class Vector {
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    moveTo(x, y) {
        this._x = x;
        this._y = y;
    }

    normalize() {
        let norm = Math.sqrt(Math.pow(this._x, 2) + Math.pow(this._y, 2));
        return Vector.div(this, norm);
    }

    // Return the angle in radians between vec2 and vec 1
    // This gives the angle [0, 2PI) that you rotate vec 1 counterclockwise by
    // to obtain vector 2.
    static angle(vec1, vec2) {
        var diff = vec2.angle() - vec1.angle();
        if (diff < 0) {
            diff += 2 * Math.PI;
        }
        return diff;
    }

    // Return the angle in radians (-PI,PI] from the x axis
    angle() {
        return Math.atan2(this.y, this.x);
    }

    scalarMul(scalar) {
        // Why the special handling for 0?
        // Because javascript apparently has -0, and Math.atan2(-0,-11) != math.atan2(0,-1)
        return new Vector(
            this._x == 0 ? 0 : this._x * scalar,
            this._y == 0 ? 0 : this._y * scalar
        );
    }

    static add(vec1, vec2) {
        return new Vector(vec1.x + vec2.x, vec1.y + vec2.y);
    }

    static sub(vec1, vec2) {
        return new Vector(vec1.x - vec2.x, vec1.y - vec2.y);
    }

    static mult(vec, scalar) {
        return new Vector(vec.x * scalar, vec.y * scalar);
    }

    static div(vec, scalar) {
        return new Vector(vec.x / scalar, vec.y / scalar);
    }
    
    plus(vec) {
        return new Vector(
            this._x + vec.x,
            this._y + vec.y
        )
    }
    
    dist2(other) {
        return Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2);
    }

    distance(other) {
        return Math.sqrt(
            Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
    }

    l1_dist(other) {
        return Math.abs(this.x - other.x) + Math.abs(this.y - other.y);
    }

    linf_dist(other) {
        return Math.max(Math.abs(this.x - other.x), Math.abs(this.y - other.y));
    }

    copy() {
        return new Vector(this.x, this.y);
    }

    transform(rotate, scale) {
        let theta = rotate * (Math.PI / 180);
        let s = Math.sin(theta);
        let c = Math.cos(theta);
        let xnew = (this.x * c - this.y * s) * scale;
        let ynew = (this.x * s + this.y * c) * scale;
        return new Vector(xnew, ynew);
    }

    reflect(axis=0) {
        if (axis=0)
            return new Vector(-this._x, this._y);
        else
            return new Vector(this._x, -this._y);
    }
}
