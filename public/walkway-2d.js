// Author: Chris Lample

class WalkwayProblem2D {

    constructor(points, v, width, height) {
        this._points = points;
        this._v = v;
        this._width = width;
        this._height = height;
    }

    get points() {
        return this._points;
    }

    get v() {
        return this._v;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }
    
    optimalWalkwayLocation() {

        //  Determine the optimal walkway location using the "smooth quasiconvex programming" technique
        // See "Quasiconvex Programming" from Eppstein (2005)
        
        let tolerance = 3; // TODO: Set some value based on width and height?
        
        // Set an initial value for a and b to begin with
        let walkway = {
            a: new Vector(0,0),
            b: new Vector(this.width, this.height)
        }

        let travelTimeDiameter = this.getTravelTimeDiameter(walkway);
        for (let i = 0; i < 3000; i++) {
            let furthestPoints = this.getFurthestPoints(walkway, tolerance);
            let improvementVector = this.getImprovementVector(walkway, furthestPoints);
            if (improvementVector == null) {
                return walkway;
            }
            let improvedWalkway = this.improveWalkway(walkway, improvementVector, furthestPoints);
            if (improvedWalkway == null) {
                return walkway;
            }
            walkway = improvedWalkway;
        }
    }

    getTravelTimeDiameter(walkway) {
        let furthestPoints = this.getFurthestPoints(walkway, 1);
        return furthestPoints[furthestPoints.length - 1].travelTime;
        
    }

    getFurthestPoints(walkway, tolerance) {
        let travelTimes = [];
        for (let i = 0; i < this.points.length - 1; i++) {
            for (let j = i; j < this.points.length; j++) {
                let travelTimeIJ = this.travelTime(walkway, this.points[i], this.points[j]);
                travelTimes.push({
                    travelTime: travelTimeIJ,
                    i: i,
                    j: j
                });
            }
        }

        travelTimes.sort((a, b) => a.travelTime - b.travelTime);
        let maxTravelTime = travelTimes[travelTimes.length - 1].travelTime;
        let threshold = maxTravelTime - tolerance;
        let index = travelTimes.findIndex(e => e.travelTime >= threshold);
        
        return travelTimes.slice(index); 
    }

    travelTime(walkway, point1, point2) {
        let a = walkway.a;
        let b = walkway.b;
        return Math.min(
            this.euclideanDistance(point1, point2),
            this.euclideanDistance(point1, a)
                + (this.euclideanDistance(a, b) / this.v)
                + this.euclideanDistance(b, point2),
            this.euclideanDistance(point1, b)
                + (this.euclideanDistance(a, b) / this.v)
                + this.euclideanDistance(a, point2)
        );
    }

    euclideanDistance(point1, point2) {
        let dx = point1.x - point2.x;
        let dy = point1.y - point2.y;
        return Math.sqrt((dx*dx) + (dy*dy));
    }

    /* 
     * Returns the "gradient" of distance with respect to a and b.
     * This is the q* vector used in Eppstein's description.
     * a will always point towards the closest point to it (say point1)
     * and b will always point towards the other point.
     * This isn't truly the gradient: if a and b are far from point1 and point2 
     * so that the walkway isn't used, then the real gradient would be 0.
     * Our custom "gradient" will work better for applying the "smooth walkway location"
     * Since we avoid local minimums.
     */
    gradient(walkway, point1, point2) {
        let a = walkway.a;
        let b = walkway.b;
        
        let pointA; // Whichever of point1 or point2 is closer to a
        let pointB;
        if (this.euclideanDistance(a, point1) <= this.euclideanDistance(a, point2)) {
            pointA = point1;
            pointB = point2;
        } else {
            pointA = point2;
            pointB = point1;
        }

        return {
            ax: pointA.x - a.x,
            ay: pointA.y - a.y,
            bx: pointB.x - b.x,
            by: pointB.y - b.y
        }
    }

    getImprovementVector(walkway, furthestPoints) {

        let inequalities = this.getInequalities(walkway, furthestPoints);
        let objectiveFunction = this.getObjectiveFunction(inequalities);
        let lp = new BoundedLinearProgram(inequalities, objectiveFunction, 100);
        
        let improvementVector = lp.solve();
        if (improvementVector == null) {
            return null;
        }
        return this.normalizeVector(improvementVector);
    }

    getInequalities(walkway, furthestPoints) {

        return furthestPoints.map(point => {
            let point1 = this.points[point.i];
            let point2 = this.points[point.j];
            let gradient = this.gradient(walkway, point1, point2);
            
            return {
                lhs: [
                    gradient.ax,
                    gradient.ay,
                    gradient.bx,
                    gradient.by
                ],
                rhs: 0
            };
        });
    }

    getObjectiveFunction(inequalities) {
        // ObjectiveFunction is the sum of the dot products
        let objectiveFunction = [0, 0, 0, 0];
        for (let inequality of inequalities) {
            for (let i = 0; i < 4; i++) {
                objectiveFunction[i] += inequality.lhs[i];
            }
        }
        return objectiveFunction;
    }

    // Normalize to a unit vector
    normalizeVector(vec) {
        let length = Math.sqrt(
            vec.reduce((sum, v) => sum + v*v, 0)
        );
        return vec.map(v => v / length);
    }

    improveWalkway(walkway, improvementVector, furthestPoints) {
        let scale = 0.1;
        let improvementVecA = new Vector(improvementVector[0], improvementVector[1]);
        let improvementVecB = new Vector(improvementVector[2], improvementVector[3]);
        let newWalkway = walkway;
        while (scale <= 3.2) {
            newWalkway = {
                a: walkway.a.plus(improvementVecA.scalarMul(scale)),
                b: walkway.b.plus(improvementVecB.scalarMul(scale))
            }

            if (!this.testFurthestPoints(furthestPoints, newWalkway)) {
                if (scale <= 0.11) {
                    return null;
                }
                return {
                    a: walkway.a.plus(improvementVecA.scalarMul(scale / 4)),
                    b: walkway.b.plus(improvementVecB.scalarMul(scale / 4))
                }
            }
            scale *= 2;
        }

        return {
            a: walkway.a.plus(improvementVecA.scalarMul(scale / 2)),
            b: walkway.b.plus(improvementVecB.scalarMul(scale / 2))
        };
    }

    testFurthestPoints(furthestPoints, newWalkway) {
        for (let point of furthestPoints) {
            let travelTime = this.travelTime(newWalkway, this.points[point.i], this.points[point.j]);
            if (travelTime > point.travelTime) {
                return false;
            }
            point.travelTime = travelTime;
        }
       
        return true;
    }
}
