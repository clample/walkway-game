// Author: Maxime Schoemans and Chris Lample

let HIGHWAY_SPEED = 2 // Default highway speed

// Draw a triangle at the given position
function drawFactory(f, color) {
    fill(color);
    let r = 12;
    let x1 = f.x;
    let y1 = f.y - r;
    let x2 = f.x + r*Math.cos(Math.PI/6);
    let y2 = f.y + r*Math.sin(Math.PI/6);
    let x3 = f.x - r*Math.cos(Math.PI/6);
    let y3 = f.y + r*Math.sin(Math.PI/6);
    triangle(x1, y1, x2, y2, x3, y3);
    fill('black');
}

// Draw a line connecting two given points
function drawHighway(t1, t2, color) {
    fill(color);
    stroke(color);
    if (t1 != null && t2 != null)
        drawLine(t1, t2);
    if (t1 != null)
        drawPoint(t1);
    if (t2 != null)
        drawPoint(t2);
    fill('black');
    stroke('black');
}

// Walkway class
class Walkway {
    constructor(a, b, v=HIGHWAY_SPEED) {
        this.a = a; // endpoint 1
        this.b = b; // endpoint 2
        this.v = v; // walkway speed
    }

    // Selects the endpoint clicked by the user
    getClosestPoint(pos, threshold=15) {
        if (pos.distance(this.a) < threshold)
            return this.a;
        else if (pos.distance(this.b) < threshold)
            return this.b;
        else
            return null;
    }

    // Used to have a uniform interface for all highways in the game class
    update() {}

    draw(color='blue') {
        drawHighway(this.a, this.b, color);
    }
}

// Center and Highway class
class CH {
    constructor(f, t1, t2, v=HIGHWAY_SPEED) {
        this.f = f; // factory
        this.t1 = t1; // endpoint 1
        this.t2 = t2; // endpoint 2
        this.l;
        this.lv;
        if (t2 != null) {
            this.l = t1.distance(t2); // length of the highway
            this.lv = this.l / v;
        }
        this.v = v; // highway speed
    }

    // Rotates and scales a factory and highway
    transform(rotate, scale) {
        let f = this.f.transform(rotate, scale);
        let t1 = this.t1.transform(rotate, scale);
        let t2 = this.t2.transform(rotate, scale);
        return new CH(f, t1, t2, this.v);
    }

    // Selects the endpoint clicked by the user
    getClosestPoint(pos, threshold=15) {
        if (pos.distance(this.f) < threshold)
            return this.f;
        else if (pos.distance(this.t1) < threshold)
            return this.t1;
        else if (pos.distance(this.t2) < threshold)
            return this.t2;
        else
            return null;
    }

    // Updates l and lv when the user modifies the factory or highway
    update() {
        this.l = this.t1.distance(this.t2);
        this.lv = this.l / this.v;
    }

    draw(color='blue') {
        drawHighway(this.t1, this.t2, color);
        drawFactory(this.f, color);
    }
}


// Center and Turnpike class
class CT extends CH {

    transform(rotate, scale) {
        let f = this.f.transform(rotate, scale);
        let t1 = this.t1.transform(rotate, scale);
        let t2 = this.t2.transform(rotate, scale);
        return new CT(f, t1, t2, this.v);
    }

    // computes l2-distance from a point to the factory
    distance(p) {
        return Math.min(
            this.f.distance(p),
            this.f.distance(this.t1) + this.lv + this.t2.distance(p),
            this.f.distance(this.t2) + this.lv + this.t1.distance(p));
    }

    // computes l1-distance from a point to the factory
    l1_dist(p) {
        return Math.min(
            this.f.l1_dist(p),
            this.f.l1_dist(this.t1) + this.lv + this.t2.l1_dist(p),
            this.f.l1_dist(this.t2) + this.lv + this.t1.l1_dist(p));
    }

    // computes linfinity-distance from a point to the factory
    linf_dist(p) {
        return Math.min(
            this.f.linf_dist(p),
            this.f.linf_dist(this.t1) + this.lv + this.t2.linf_dist(p),
            this.f.linf_dist(this.t2) + this.lv + this.t1.linf_dist(p));
    }
}

// Optimal Center and Turnpike class
class OptCT extends CH {

    // for and optimal turnpike:
    // f = t1, t = t2
    constructor(f, t, v=HIGHWAY_SPEED) {
        super(f, f, t, v);
    }

    get t() {return this.t2;}
    set t(t) {this.t2 = t;}

    transform(rotate, scale) {
        let f = this.f.transform(rotate, scale);
        let t = null;
        if (this.t != null)
            t = this.t.transform(rotate, scale);
        return new OptCT(f, t, this.v);
    }

    // computes l2-distance from a point to the factory
    distance(p) {
        if (this.t == null)
            return this.f.distance(p);
        return Math.min(
            this.f.distance(p), 
            this.t.distance(p) + this.lv);
    }

    // computes l1-distance from a point to the factory
    l1_dist(p) {
        if (this.t == null)
            return this.f.l1_dist(p);
        return Math.min(
            this.f.l1_dist(p),
            this.t.l1_dist(p) + this.lv);
    }

    // computes linfinity-distance from a point to the factory
    linf_dist(p) {
        if (this.t == null)
            return this.f.linf_dist(p);
        return Math.min(
            this.f.linf_dist(p),
            this.t.linf_dist(p) + this.lv);
    }

    draw(color='blue') {
        if (this.t != null)
            drawHighway(this.t1, this.t2, color);
        drawFactory(this.f, color);
    }
}


// Center and Freeway class
class CF extends CH {

    constructor(f, t1, t2, v=HIGHWAY_SPEED) {
        super(f, t1, t2, v);
        let diff = Vector.sub(t2, t1)
        this.angle = Math.atan(diff.y / diff.x); // angle between the freeway end the x-axis
        this.angle_case = null;
        if (Math.abs(this.angle) < Math.PI / 4)
            this.angle_case = 0;
        else if (Math.abs(this.angle) > Math.PI / 4)
            this.angle_case = 1;
        else
            this.angle_case = 2;
        this.factory_entrance = this.get_freeway_entrance(this.f); // closest point on the freeway from the factory
    }

    // Update l, lv, angle and factory entrance 
    // when the user modifies the factory or freeway
    update() {
        this.l = this.t1.distance(this.t2);
        this.lv = this.l / this.v;
        let diff = Vector.sub(this.t2, this.t1);
        this.angle = Math.atan(diff.y / diff.x);
        this.angle_case = null;
        if (Math.abs(this.angle) < Math.PI / 4)
            this.angle_case = 0;
        else if (Math.abs(this.angle) > Math.PI / 4)
            this.angle_case = 1;
        else
            this.angle_case = 2;
        this.factory_entrance = this.get_freeway_entrance(this.f);
    }

    transform(rotate, scale) {
        let f = this.f.transform(rotate, scale);
        let t1 = this.t1.transform(rotate, scale);
        let t2 = this.t2.transform(rotate, scale);
        return new CF(f, t1, t2, this.v);
    }

    // Reflect the factory and freeway along a given axis
    // 0 == reflect along y-axis
    // 1 == reflect along x-axis
    reflect(axis=0) {
        let f = this.f.reflect(axis);
        let t1 = this.t1.reflect(axis);
        let t2 = this.t2.reflect(axis);
        return new CF(f, t1, t2, this.v);
    }

    // Get the closest point on the freeway from the given point.
    // If the freeway is at 45 degrees, 
    // there can exist a segment of closest points on the freeway.
    // In that case, return the endpoints of that segment sorted by x-coordinate.
    get_freeway_entrance(p) {
        let closest_pos = null;
        switch (this.angle_case) {
            case 0:
                if (p.x < Math.min(p.x, this.t2.x)) { // enpoint with smallest x-coordinate
                    if (this.t1.x < this.t2.x)
                        closest_pos = this.t1;
                    else
                        closest_pos = this.t2;
                } else if (p.x > Math.max(this.t1.x, this.t2.x)) { // enpoint with largest x-coordinate
                    if (this.t1.x < this.t2.x)
                        closest_pos = this.t2;
                    else
                        closest_pos = this.t1;
                } else { // Vertical projection of the point onto the freeway
                    let x = p.x; // = x1*(1-t) + x2*t => t = (x - x1) / (x2 - x1)
                    let y = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x); // = y1*(1-t) + y2*t = y1*((x2 - x1) - (x - x1))/(x2 - x1) + y2*(x - x1)/(x2 - x1) = (y1*(x2 - x) + y2*(x - x1))/(x2 - x1)
                    closest_pos = new Vector(x, y);
                }
                break;
            case 1:
                if (p.y < Math.min(this.t1.y, this.t2.y)) { // enpoint with smallest y-coordinate
                    if (this.t1.y < this.t2.y)
                        closest_pos = this.t1;
                    else
                        closest_pos = this.t2;
                } else if (p.y > Math.max(this.t1.y, this.t2.y)) { // enpoint with largest y-coordinate
                    if (this.t1.y < this.t2.y)
                        closest_pos = this.t2;
                    else
                        closest_pos = this.t1;
                } else { // Horizontal projection of the point onto the freeway
                    let y = p.y;
                    let x = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                    closest_pos = new Vector(x, y);
                }
                break;
            case 2:
                // Either the closest point is one of the enpoints,
                // or there exists a segment of closets points.
                // In that case, project the point onto the line going through the freeway
                // (capping at the endpoints if the projection is outside the freeway)
                // Then return both projections sorted by x-coordinate
                if (this.angle > 0) {
                    let min_t;
                    let max_t;
                    if (this.t1.x < this.t2.x) {
                        min_t = this.t1;
                        max_t = this.t2;
                    } else {
                        min_t = this.t2;
                        max_t = this.t1;
                    }
                    if (p.x < min_t.x && p.y < min_t.y) { // 
                        closest_pos = min_t
                    } else if (p.x > max_t.x && p.y > max_t.y) {
                        closest_pos = max_t
                    } else if ((p.x < min_t.x && p.y > max_t.y)
                            || (p.x > max_t.x && p.y < min_t.y)) {
                        closest_pos = [min_t, max_t];
                    } else if (p.x > max_t.x) {
                        let y = p.y;
                        let x = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                        closest_pos = [new Vector(x, y), max_t];
                    } else if (p.y > max_t.y) {
                        let x = p.x;
                        let y = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x);
                        closest_pos = [new Vector(x, y), max_t];
                    } else if (p.x < min_t.x) {
                        let y = p.y;
                        let x = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                        closest_pos = [min_t, new Vector(x, y)];
                    } else if (p.y < min_t.y) {
                        let x = p.x;
                        let y = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x);
                        closest_pos = [min_t, new Vector(x, y)];
                    } else {
                        let x1 = p.x;
                        let y1 = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x);
                        let y2 = p.y;
                        let x2 = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                        if (x1 < x2) 
                            closest_pos = [new Vector(x1, y1), new Vector(x2, y2)]
                        else
                            closest_pos = [new Vector(x2, y2), new Vector(x1, y1)]
                    }
                } else {
                    let min_t;
                    let max_t;
                    if (this.t1.x < this.t2.x) {
                        min_t = this.t1;
                        max_t = this.t2;
                    } else {
                        min_t = this.t2;
                        max_t = this.t1;
                    }
                    if (p.x < min_t.x && p.y > min_t.y) {
                        closest_pos = min_t
                    } else if (p.x > max_t.x && p.y < max_t.y) {
                        closest_pos = max_t
                    } else if ((p.x < min_t.x && p.y < max_t.y)
                            || (p.x > max_t.x && p.y > min_t.y)) {
                        closest_pos = [min_t, max_t];
                    } else if (p.x > max_t.x) {
                        let y = p.y;
                        let x = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                        closest_pos = [new Vector(x, y), max_t];
                    } else if (p.y < max_t.y) {
                        let x = p.x;
                        let y = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x);
                        closest_pos = [new Vector(x, y), max_t];
                    } else if (p.x < min_t.x) {
                        let y = p.y;
                        let x = (this.t1.x*(this.t2.y - y) + this.t2.x*(y - this.t1.y))/(this.t2.y - this.t1.y);
                        closest_pos = [min_t, new Vector(x, y)];
                    } else if (p.y > min_t.y) {
                        let x = p.x;
                        let y = (this.t1.y*(this.t2.x - x) + this.t2.y*(x - this.t1.x))/(this.t2.x - this.t1.x);
                        closest_pos = [min_t, new Vector(x, y)];
                    } else {
                        let x1 = p.x;
                        let y1 = (this.t1.y*(this.t2.x - x1) + this.t2.y*(x1 - this.t1.x))/(this.t2.x - this.t1.x);
                        let y2 = p.y;
                        let x2 = (this.t1.x*(this.t2.y - y2) + this.t2.x*(y2 - this.t1.y))/(this.t2.y - this.t1.y);
                        if (x1 < x2) 
                            closest_pos = [new Vector(x1, y1), new Vector(x2, y2)]
                        else
                            closest_pos = [new Vector(x2, y2), new Vector(x1, y1)]
                    }
                }
                break;
        }
        return closest_pos;
    }

    // computes l1-distance from a point to the factory
    l1_dist(p) {
        let entrance = this.get_freeway_entrance(p);
        if (this.angle_case == 2) {
            if (entrance instanceof Vector && this.factory_entrance instanceof Vector) {
                return Math.min(
                    this.f.l1_dist(p), 
                    this.f.l1_dist(this.factory_entrance) 
                    + this.factory_entrance.distance(entrance) / this.v 
                    + entrance.l1_dist(p)
                );
            } else if (entrance instanceof Vector) {
                if (entrance.x < this.factory_entrance[0].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance[0]) 
                        + this.factory_entrance[0].distance(entrance) / this.v 
                        + entrance.l1_dist(p)
                    );
                } else if (entrance.x > this.factory_entrance[1].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance[1]) 
                        + this.factory_entrance[1].distance(entrance) / this.v 
                        + entrance.l1_dist(p)
                    );
                } else {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(entrance) 
                        + entrance.l1_dist(p)
                    );
                }
            } else if (this.factory_entrance instanceof Vector) {
                if (this.factory_entrance.x < entrance[0].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance) 
                        + this.factory_entrance.distance(entrance[0]) / this.v 
                        + entrance[0].l1_dist(p)
                    );
                } else if (this.factory_entrance.x > entrance[1].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance) 
                        + this.factory_entrance.distance(entrance[1]) / this.v 
                        + entrance[1].l1_dist(p)
                    );
                } else {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance) 
                        + this.factory_entrance.l1_dist(p)
                    );
                }
            } else {
                if (entrance[1].x < this.factory_entrance[0].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance[0]) 
                        + this.factory_entrance[0].distance(entrance[1]) / this.v 
                        + entrance[1].l1_dist(p)
                    );
                } else if (entrance[0].x > this.factory_entrance[1].x) {
                    return Math.min(
                        this.f.l1_dist(p), 
                        this.f.l1_dist(this.factory_entrance[1]) 
                        + this.factory_entrance[1].distance(entrance[0]) / this.v 
                        + entrance[0].l1_dist(p)
                    );
                } else {
                    if (entrance[0].x < this.factory_entrance[0].x) {
                        return Math.min(
                            this.f.l1_dist(p), 
                            this.f.l1_dist(this.factory_entrance[0]) 
                            + this.factory_entrance[0].l1_dist(p)
                        );
                    } else {
                        return Math.min(
                            this.f.l1_dist(p), 
                            this.f.l1_dist(entrance[0]) 
                            + entrance[0].l1_dist(p)
                        );
                    }
                }
            }
        } else {
            return Math.min(
                this.f.l1_dist(p), 
                this.f.l1_dist(this.factory_entrance) 
                + this.factory_entrance.distance(entrance) / this.v 
                + entrance.l1_dist(p)
            );
        }
    }
}


// Optimal Center and Freeway
// not really needed, but kept for uniformity
class OptCF extends CF {

    transform(rotate, scale) {
        let f = this.f.transform(rotate, scale);
        let t1 = this.t1.transform(rotate, scale);
        let t2 = this.t2.transform(rotate, scale);
        return new OptCF(f, t1, t2, this.v);
    }

    reflect(axis=0) {
        let f = this.f.reflect(axis);
        let t1 = this.t1.reflect(axis);
        let t2 = this.t2.reflect(axis);
        return new OptCF(f, t1, t2, this.v);
    }
}
