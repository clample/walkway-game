// Author: Maxime Schoemans

// Class implementing the solution to the 1-Center and 1-Turnpike problem
class OneCenterOneTurnpikeProblem {

    // Solve the complete problem
    // If length is null, solve the variable-length problem (this is not completely implemented)
    // Else, solve the fixed-length problem
    static solve(points, v, length=null) {
        let pointset = new PointSet(points);
        let result = null;
        let best_R = Infinity;
        // Test case without highway
        let rotate = 45;
        let scale = Math.sqrt(2);
        let ps = pointset.transform(rotate, scale);
        let new_full_res = OneCenterProblem.solve(ps);
        let new_result = new_full_res.c.transform(-rotate, 1/scale);

        let new_R = new_full_res.R;
        if (new_R < best_R) {
            best_R = new_R;
            result = new OptCT(new_result, null, null);
        }
        // Test cases with highway
        if (length != null)
            length = length * scale;
        for (var i = 0; i < 4; i++) {
            rotate = 45 + i*90;
            ps = pointset.transform(rotate, scale);
            new_full_res = this.solveUpperLeft(ps, v, length)
            new_result = new_full_res.ct.transform(-rotate, 1/scale);
            new_R = new_full_res.R;
            if (new_R < best_R) {
                best_R = new_R;
                result = new_result;
            }
        }
        return {'ct': result, 'R': best_R};
    }

    /* Solve the rectilinear 1-center and 1-turnpike problem
     * assuming the factory is on the top-left of t
     * best theoretical complexity: O(n²)
     * implemented complexity: O(n³)
     * The implemented complexity is higher, 
     * just to have cleaner and more readable code
     */
    static solveUpperLeft(pointset, v, length=null) {
        let n = pointset.length;

        let pointset_sorted_x = pointset.copy()
        pointset_sorted_x.points.sort(function(a, b) {return a.x - b.x;});
        let l = []
        let r = []
        for (var i = 0; i < n-1; i++) {
            let li_points = pointset_sorted_x.subSet(0, i+1);
            let ri_points = pointset_sorted_x.subSet(i+1, n);
            l.push(li_points.extremeSet());
            r.push(ri_points.extremeSet());
        };

        let xp = pointset.sortedX();
        let yq = pointset.sortedY();
        let ur = [];
        let dl = [];
        for (var i = 0; i < yq.length; i++) {
            let uri = [];
            let dli = [];
            // To reduce the complexity of the algorithm to O(n²),
            // modify this loop to compute all extreme sets in 1 forward
            // and 1 backward pass. This requires to maintain a list of max/min for
            // the x and y values for each value of j.
            // This was not implemented, just to keep the code clean.
            for (var j = 0; j < xp.length; j++) {
                let urij_points = [];
                let dlij_points = [];
                for (var k = 0; k < pointset.points.length; k++) {
                    let p = pointset.points[k];
                    if (p.y < yq[i] && p.x < xp[j])
                        urij_points.push(p.copy());
                    else
                        dlij_points.push(p.copy());
                }
                if (urij_points.length == 0 || dlij_points.length == 0) {
                    uri.push(null);
                    dli.push(null);
                } else {
                    uri.push(new PointSet(urij_points).extremeSet());
                    dli.push(new PointSet(dlij_points).extremeSet());
                }
            }
            ur.push(uri);
            dl.push(dli);
        }

        let solved_problems = new Set();

        let result = null;
        let best_R = Infinity;
        let new_full_res = null;
        let new_result = null;
        let new_R = null;

        for (var i = 0; i < n-1; i++) {
            let problem_string = l[i].hash().concat(r[i].hash());
            if (solved_problems.has(problem_string))
                continue;
            else
                solved_problems.add(problem_string);
            new_full_res = this.solveBasic(l[i], r[i], v, length);
            new_result = new_full_res.segment;
            new_R = new_full_res.R;
            if (new_R < best_R) {
                best_R = new_R;
                result = new_result;
            }
        }

        for (var i = 0; i < yq.length; i++) {
            for (var j = 0; j < xp.length; j++) {
                if (ur[i][j] == null)
                    continue;
                let problem_string = ur[i][j].hash().concat(dl[i][j].hash());
                if (solved_problems.has(problem_string))
                    continue;
                else
                    solved_problems.add(problem_string);
                new_full_res = this.solveBasic(ur[i][j], dl[i][j], v, length);
                new_result = new_full_res.segment;
                new_R = new_full_res.R;
                if (new_R < best_R) {
                    best_R = new_R;
                    result = new_result;
                }
            }
        }
        return {'ct': new OptCT(result.f, result.t, v), 'R': best_R};
    }

    // Solve the basic problem given two extreme sets.
    static solveBasic(W, H, v, l) {
        if (l == null)
            return this.solveBasicVL(W, H, v);
        else
            return this.solveBasicFL(W, H, v, l);
    }

    // Solve the variable length variant of the basic problem
    static solveBasicVL(W, H, v) {
        Error("Not implemented yet");
    }

    // Solve the fixed length variant of the basic problem
    static solveBasicFL(W, H, v, l) {
        let eps1 = Math.max(0, H.delta + (l / (v*Math.sqrt(2))) - W.delta);
        let eps2 = Math.max(0, W.delta - (l / (v*Math.sqrt(2))) - H.delta);
        let cw = W.center(W.delta + eps1);
        let ch = H.center(H.delta + eps2);
        if (cw.min_dist(ch) <= l && cw.max_dist(ch) >= l) { // Solution can be found directly
            return {'segment': Center.findSegment(cw, ch, l), 
                    'R': W.delta + eps1};
        } else {                                            // We have to search for a value x
            let x = 1;
            cw = W.center(W.delta + eps1 + x)
            ch = H.center(H.delta + eps2 + x)
            while (!(cw.min_dist(ch) < l && cw.max_dist(ch) > l)) { // Double x as long as the condition is not satisfied
                cw = W.center(W.delta + eps1 + x);
                ch = H.center(H.delta + eps2 + x);
                x = x*2;
            }
            let min_x = Math.min(x/2, x-1);
            let max_x = x;
            let best_x = x;
            let best_R = W.delta + eps1 + best_x;
            while (max_x - min_x > 1e-6) { // Binary search on the x as long until wwe reach the desired precision
                let middle = (max_x + min_x)/2;
                cw = W.center(W.delta + eps1 + middle);
                ch = H.center(H.delta + eps2 + middle);
                if (cw.min_dist(ch) < l && cw.max_dist(ch) > l) {
                    max_x = middle;
                    best_x = max_x;
                    best_R = W.delta + eps1 + best_x;
                } else {
                    min_x = middle;
                }
            }
            // Compute the optimal location given the computed x
            cw = W.center(W.delta + eps1 + best_x);
            ch = H.center(H.delta + eps2 + best_x);
            return {'segment': Center.findSegment(cw, ch, l), 
                    'R': best_R};
        }
    }
}