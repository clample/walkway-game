// Author: Maxime Schoemans

// Box class
class Box {
    constructor(minX, maxX, minY, maxY) {
        this.minX = minX
        this.maxX = maxX
        this.minY = minY
        this.maxY = maxY
        this.deltaX = 0.5 * (this.maxX - this.minX) // Half of the width of the box
        this.deltaY = 0.5 * (this.maxY - this.minY) // Half of he height of the box
        this.delta = Math.max(this.deltaX, this.deltaY) // Half of the maximum linfinity distance between two points in the box.
    }

    // center of the box
    centerPoint() {
        let x = 0.5 * (this.maxX + this.minX);
        let y = 0.5 * (this.maxY + this.minY);
        return new Vector(x, y);
    }
}

// Extreme Set class
class ExtremeSet extends Box {

    // Used to test easily if the same extreme set
    // was already encountered before
    hash() {
        return "".concat(
            String(this.minX), 
            String(this.maxX), 
            String(this.minY),
            String(this.maxY));
    }

    // Locus of the centers of the axis-parallel squares
    // of a given radius (i.e., L∞ balls) that cover the extreme set.
    // Defined in the 1-Center and 1-Highway Problem paper.
    center(radius) {
        return new Center(
            this.maxX - radius, 
            this.minX + radius,
            this.maxY - radius, 
            this.minY + radius);
    }
}

// Center class
class Center extends Box {

    corners() {
        if (this.deltaX < 0 && this.deltaY < 0) {
            return [];
        } else {
            return [
                new Vector(this.minX, this.minY),
                new Vector(this.maxX, this.minY),
                new Vector(this.maxX, this.maxY),
                new Vector(this.minX, this.maxY)
            ];
        };
    }

    // Returns the minimum distance between two centers
    // Returns 0 if the boxes overlap
    min_dist(other) {
        let outer = new Box(
            Math.min(this.minX, other.minX), 
            Math.max(this.maxX, other.maxX), 
            Math.min(this.minY, other.minY), 
            Math.max(this.maxY, other.maxY));
        let inner_deltaX = Math.max(0, outer.deltaX - this.deltaX - other.deltaX);
        let inner_deltaY = Math.max(0, outer.deltaY - this.deltaY - other.deltaY);
        return Math.sqrt(
            Math.pow(inner_deltaX, 2) + Math.pow(inner_deltaY, 2)
            ) * 2;
    }

    // Returns the two points of each center that are closest to each other
    // If the centers overlap, returns the center of their union.
    min_dist_points(other) {
        let outer = new Box(
            Math.min(this.minX, other.minX), 
            Math.max(this.maxX, other.maxX), 
            Math.min(this.minY, other.minY), 
            Math.max(this.maxY, other.maxY));
        let inner_deltaX = outer.deltaX - this.deltaX - other.deltaX;
        let inner_deltaY = outer.deltaY - this.deltaY - other.deltaY;
        if (inner_deltaX <= 1e-6 && inner_deltaY <= 1e-6) {
            let x = Math.max(this.minX, other.minX) - inner_deltaX;
            let y = Math.max(this.minY, other.minY) - inner_deltaY;
            return [new Vector(x, y), new Vector(x, y)];
        } else if (inner_deltaX <= 1e-6) {
            let x = Math.max(this.minX, other.minX) - inner_deltaX;
            if (this.maxY < other.minY) {
                return [new Vector(x, this.maxY), new Vector(x, other.minY)];
            } else {
                return [new Vector(x, this.minY), new Vector(x, other.maxY)];
            }
        } else if (inner_deltaY <= 1e-6) {
            let y = Math.max(this.minY, other.minY) - inner_deltaY;
            if (this.maxX < other.minX) {
                return [new Vector(this.maxX, y), new Vector(other.minX, y)];
            } else {
                return [new Vector(this.minX, y), new Vector(other.maxX, y)];
            }
        } else {
            if (this.maxX < other.minX) {
                if (this.maxY < other.minY) {
                    return [new Vector(this.maxX, this.maxY), new Vector(other.minX, other.minY)];
                } else {
                    return [new Vector(this.maxX, this.minY), new Vector(other.minX, other.maxY)];
                }
            } else {
                if (this.maxY < other.minY) {
                    return [new Vector(this.minX, this.maxY), new Vector(other.maxX, other.minY)];
                } else {
                    return [new Vector(this.minX, this.minY), new Vector(other.maxX, other.maxY)];
                }
            }
        }
    }

    // Returns the maximum distance between two centers
    // Does not handle the case of centers contained in each other
    max_dist(other) {
        let max_dist2 = -Infinity;
        let this_corners = this.corners();
        let other_corners = other.corners();
        for (var i = 0; i < this_corners.length; i++) {
            for (var j = 0; j < other_corners.length; j++) {
                max_dist2 = Math.max(max_dist2, this_corners[i].dist2(other_corners[j]));
            }
        }
        return Math.sqrt(max_dist2);
    }

    // Returns the two points of each center that are farthest from each other
    // Does not handle the case of centers contained in each other
    max_dist_points(other) {
        let max_dist = -Infinity;
        let this_max = null;
        let other_max = null;
        let this_corners = this.corners();
        let other_corners = other.corners();
        for (var i = 0; i < this_corners.length; i++) {
            for (var j = 0; j < other_corners.length; j++) {
                if (this_corners[i].dist2(other_corners[j]) > max_dist) {
                    max_dist = this_corners[i].dist2(other_corners[j]);
                    this_max = this_corners[i];
                    other_max = other_corners[j];
                }
            }
        }
        return [this_max.copy(), other_max.copy()];
    }

    // Finds a segment of a given length starting in center 1 and ending in center 2
    // Returns two positions {f, t}, with f being in center 1 and t in center 2.
    // Assumes that this segment exists
    static findSegment(c1, c2, l) {
        let min_points = c1.min_dist_points(c2);
        let max_points = c1.max_dist_points(c2);
        let c1_min = min_points[0];
        let c2_min = min_points[1];
        let c1_max = max_points[0];
        let c2_max = max_points[1];
        let min_dist = c1_min.distance(c2_min);
        let max_dist = c1_max.distance(c2_max);
        let theta = (l - min_dist)/(max_dist - min_dist);
        let f = Vector.add(Vector.mult(c1_min, 1 - theta), Vector.mult(c1_max, theta));
        let t = Vector.add(Vector.mult(c2_min, 1 - theta), Vector.mult(c2_max, theta));
        let d = f.distance(t);
        while (Math.abs(d - l) > 1e-6) {
            if (d > l) {
                c1_max = f;
                c2_max = t;
                max_dist = d;
            } else {
                c1_min = f;
                c2_min = t;
                min_dist = d;
            }
            theta = (l - min_dist)/(max_dist - min_dist);
            f = Vector.add(Vector.mult(c1_min, 1 - theta), Vector.mult(c1_max, theta));
            t = Vector.add(Vector.mult(c2_min, 1 - theta), Vector.mult(c2_max, theta));
            d = f.distance(t);
        }
        return {'f': f, 't': t};
    }
}