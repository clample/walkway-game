<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Transport Game</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/p5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/addons/p5.dom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/addons/p5.sound.min.js"></script>
    <script src="./bounded-lp.js"></script>
    <script src="./convex-hull.js"></script>
    <script src="./utils.js"></script>
    <script src="./vector.js"></script>
    <script src="./walkway-1d.js"></script>
    <script src="./walkway-2d.js"></script>

    <script src="walkways.js"></script>
  </head>
  <body>
      <nav>
        <h1>Transport Game</h1>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="https://gitlab.com/clample/walkway-game">Source Code</a></li>
        </ul>
      </nav>
      <h3>Author: Chris Lample</h3>
      <h2>Introduction</h2>
      <p>A <i>walkway</i> is a transportation device introduced in [1] which speeds up travel between two points. Just like the walkways commonly found in airports, it is only possible to enter and exit the walkway at its endpoints. Furthermore, the walkway can be used to travel in either direction. Given a set of points, it is interesting to consider the placement of a walkway which minimizes the maximum travel time between two points.<br><br>
      For the walkway game, I've implemented travel time diameter and optimal walkway placement algorithms for the 1-dimensional case, as described in [1]. To solve the optimal walkway placement problem for the 2-dimensional case, I've used smooth quasiconvex programming as described in [2]. The transport game then uses these algorithms to display an optimal walkway to the user. We then also compare the travel time diameters of the user's guess and the optimal position.</p>
      <h2>1-Dimensional Case</h2>
      <div class="p5-wrapper">
        <div id="one-d-walkway-canvas"></div>
        <div class="walkway-buttons">
          <button onclick="requestOneDOptimal()">Calculate Optimal</button>
          <button onclick="clearOneD()">Clear</button>
        </div>
        <div id="one-d-travel-diameter"></div>
      </div>
      <div class="caption">
        <i>Double click to add points. Drag points by holding down the mouse.</i>
      </div>
      <p>Since the number of points used in the game is quite small, a brute force \(O(n^2)\) algorithm for calculating the travel time diameter would work fine. Out of interest, though, I've implemented the \(O(n~log(n))\) algorithm described in [1]. The algorithm splits the set of points into four seperate sections: those to the left and right of the walkway, and those on the first and second halfs of the walkway. We can then consider the different cases for travel between two points. Most of the cases were straightforward to implement, but the case with points on different halfs of the walkway was particularly interesting. As described in [1], the algorithm maps these points onto a circle and leaves an arc of size \(\frac{b-a}{v}\), where a is the start of the walkway, b is the end, and v is the speed of the walkway. Finding the furthest two points then corresponds to finding the largest angle between two points. For any two points on the circle, there are two angles between them. Both of these angles have a meaning in the context of the problem. When traveling between two points, the angle containing the arc corresponds to taking the walkway and the angle not containing the arc corresponds to not taking the walkway. Since the angle between two points on the circle is proportional to their distance and because the points form a convex hull, we can apply the algorithm for computing the convex hull diameter discussed in [3].</p>

      <p>In addition to computing the travel time diameter, I've also implemented the optimal walkway placement algorithm from [1]. The implementation was straightforward and followed the description exactly. Since there are only two possible positions for the optimal walkway, the algorithm simply tests both cases.</p>

      <h2>2-Dimensional Case</h2>
      <div class="p5-wrapper">
        <div id="two-d-walkway-canvas"></div>
        <div class="walkway-buttons">
          <button onclick="requestTwoDOptimal()">Calculate Optimal</button>
          <button onclick="clearTwoD()">Clear</button>
        </div>
        <div id="two-d-travel-diameter"></div>
      </div>
      <div class="caption">
        <i>Double click to add points. Drag points by holding down the mouse.</i>
      </div>
            
      <p>The implementation of the 2-dimensional case was significantly more complex than that of the 1-dimensional case. [1] develops an \(O(n~log(n))\) algorithm for computing the travel time diameter. For the game, however, I simply calculate the travel time between all pairs of points, giving an \(O(n^2)\) algorithm. This is sufficient for the game since we have a small number of points. Given more time, though, it would certainly be interesting to implement the faster algorithm.</p>

      <p>To provide a solution to the optimal walkway problem in the 2-dimensional case, the authors of [1] show that this is an <i>implicit quasiconvex program</i> and can therefore be solved with an existing algorithm. Rather than solving the optimal walkway problem in this way, though, my implementation uses the <i>smooth quasiconvex programming</i> technique introduced in [2]. I chose this approach because it seemed more straightforward to implement. For each pair of points \(p_1\) and \(p_2\), there is a corresponding distance function which depends on the walkway position: \(d_{p_1p_2}(a, b)\). In order to lower \(d_{p_1p_2}\) the walkway points \(a\) and \(b\) need to move towards the points \(p_1\) and \(p_2\). We can call this corresponding improvement direction \(d_{p_1p_2}^*\). To calculate the optimal walkway position, the algorithm calculates the furthest pairs of points, calculates a single improvement direction from the individual \(d_{p_1p_2}^*\) vectors, and moves the walkway slightly in this improvement direction. This process iterates until it is not possible determine an improvement direction, or the improvements become small. </p>

      <p>Calculating the single improvement direction from the individual \(d_{p_1p_2}^*\) values involves solving a system of linear equations. The dot product of the final improvement vector with each \(d_{p_1p_2}^*\) must be greater than or equal to 0, otherwise it will increase the travel time diameter. This produces a single inequality for each \(d_{p_1p_2}^*\) we consider. To solve the system of linear equations, we used the technique from Chapter 4 of our textbook [4]. Since Maxime's portion of the project also required a linear program solver, we collaborated on this.</p>

      <h2>References</h2>
      <ul>
        <li>[1] Jean Cardinal, Sébastien Collette, Ferran Hurtado, Stefan Langerman,
          and Belén Palop. Optimal location of transportation devices. Computational geometry, 41(3):219–229, 2008.</li>
        <li>[2] David Eppstein. Quasiconvex programming. Combinatorial and Computational Geometry, 52(287-331):3, 2005.</li>
        <li>[3] G. T. Toussaint. Solving geometric problems with the “rotating calipers”. In Proc. MELECON, 1983.</li>
        <li>[4] Mark de Berg, Otfried Cheong, Marc van Kreveld, Marc Overmars. Computational Geometry (3rd Edition). 2008.</li>
      </ul>
  </body>
</html>
