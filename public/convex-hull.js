// Author: Chris Lample

class ConvexHull {
    
    // Set of points of the convex hull, sorted counterclockwise
    // Should be at least 3 points.
    constructor(points) {
        this._convexHullPoints = points;
    }

    get convexHullPoints() {
        return this._convexHullPoints;
    }

    size() {
        return this.convexHullPoints.length;
    }
    
    ray(i) {
        var point1 = this.point(i);
        var point2 = this.point(i+1);
        return new Vector(point2.x - point1.x, point2.y - point1.y);
    }

    point(i) {
        return this.convexHullPoints[this.mod(i, this.size())];
    }

    antipodalVertices() {
        // See rotating-calipers.pdf
        // See Shamos thesis pg 78 (it looks like Shamos has bugs)
        // See http://cgm.cs.mcgill.ca/~athens/cs507/Projects/2000/MS/diameter/document.html
        var i = 0;
        var startingI = 0;
        var bottomCaliper = this.ray(i)
        var topCaliper = bottomCaliper.scalarMul(-1);
        var j = 1;
        // With the polygon laying flat along bottomCaliper, find the top point (where topCaliper touches)
        while (Vector.angle(this.ray(i), this.ray(j)) < Math.PI) {
            j++;
        }
        var startingJ = j;
        
        var antipodalPairs = [];
        // Roll the convex polygon clockwise and record all antipodal pairs
        // At least one of the calipers will always be flush against an edge
        // We stop once the polygon has rolled 180 degrees
        while (j != startingI || i != startingJ) {
            antipodalPairs.push([i,j])
            var bottomAngle = Vector.angle(bottomCaliper, this.ray(i));
            var topAngle = Vector.angle(topCaliper, this.ray(j));
            if (bottomAngle > topAngle) {
                // bottomCaliper = this.ray(i);
                // topCaliper = bottomCaliper.scalarMul(-1);
                j = this.mod(j+1, this.size());
            } else if (topAngle > bottomAngle) {
                // topCaliper = this.ray(j);
                // bottomCaliper = topCaliper.scalarMul(-1);
                i = this.mod(i+1, this.size());
            } else {
                antipodalPairs.push([this.mod(i+1, this.size()), j]);
                antipodalPairs.push([i, this.mod(j+1, this.size())]);
                i = this.mod(i+1, this.size());
                j = this.mod(j+1, this.size());
            }

            // update the calipers
            var newBottomAngle = Vector.angle(bottomCaliper, this.ray(i));
            var newTopAngle = Vector.angle(topCaliper, this.ray(j));
            if (newBottomAngle > newTopAngle) {
                topCaliper = this.ray(j);
                bottomCaliper = topCaliper.scalarMul(-1);
            } else if (newTopAngle > newBottomAngle) {
                bottomCaliper = this.ray(i);
                topCaliper = bottomCaliper.scalarMul(-1);
            } else {
                bottomCaliper = this.ray(i);
                topCaliper = this.ray(j);
            }
        }
        return antipodalPairs;
    }

    // Find the furthest point on the convex hull from point
    // This runs in O(n), but could likely be made O(log(n)) since points are sorted
    furthestPointFrom(point) {
        var furthestPoint = null;
        var furthestPointDistance = null;
        for (let i=0; i < this.size(); i++) {
            var cvxHullPoint = this.point(i);
            // No need to take the square root
            // sqrt(x1^2 + y1^2) > sqrt(x2^2 + y2^2)
            // => x1^2 + y1^2 > x2^2 + y2^2
            var distance =
                Math.pow(point.x - cvxHullPoint.x, 2) +
                Math.pow(point.y - cvxHullPoint.y, 2);
            if (furthestPoint == null || distance > furthestPointDistance) {
                furthestPoint = i;
                furthestPointDistance = distance;
            }
        }
        return furthestPoint;
    }p

    // Javascript will return negative values for "%". For example -1 % 2 = -1.
    // For our purposes, we would like only positive values. For example -1 % 2 = 1.
    mod(n, m) {
        return ((n % m) + m) % m;
    }

}
