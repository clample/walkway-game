var assert = chai.assert;

suite('1 dimensional walkway', () => {
    setup(() => {});
    
    suite('travelTime()', () => {
	test('travel time with useless walkway', () => {
            let walkwayProb = new WalkwayProblem1D([], 1);
	    // when v=1, the walkway is no faster than normal walking
	    let uselessWalkway = {'a': 0.5, 'b': 1};
	    assert.equal(3, walkwayProb.travelTimeDistance(0, 3, uselessWalkway));
	});

	test('travel time with reversed points', () => {
            let walkwayProb = new WalkwayProblem1D([], 1);
	    let uselessWalkway = {'a': 0.5, 'b': 1};
	    assert.equal(3, walkwayProb.travelTimeDistance(2, -1, uselessWalkway));
	});

	test('travel time with walkway', () => {
            let walkwayProb = new WalkwayProblem1D([], 10);
	    // traversing the walkway takes 1
	    let walkway = {'a': 10, 'b': 20};
	    assert.equal(21, walkwayProb.travelTimeDistance(0, 30, walkway));
	});
    });

    suite('travelTimeDiameter()', () => {
	test('travel time diameter with type I-IV points', () => {
            let walkwayProb = new WalkwayProblem1D([-100, 0, 400, 100, 24, -400], 10);
	    let walkway = {'a': 10, 'b': 20};
	    // simply the distance between the two furthest points
	    let expected = walkwayProb.travelTimeDistance(400, -400, walkway);
	    assert.equal(expected, walkwayProb.travelTimeDiameter(walkway).d);
	});

        test('travel time with random points', () => {
            let walkwayProb = new WalkwayProblem1D([0,1,2,10,12,15,16,19,25], 10);
            let walkway = {'a': 10, 'b':20};
            assert.deepEqual(
                walkwayProb.travelTimeDiameterBruteForce(walkway),
                walkwayProb.travelTimeDiameter(walkway)
            );
        });
    });

    suite('maxDistanceSameHalf()', () => {
        test('all points in first half', () => {
            let walkwayProb = new WalkwayProblem1D([-100, -90, 1], 20);
            let walkway = {'a': 0, 'b': 2};
            assert.equal(101, walkwayProb.maxDistanceSameHalf(walkway).d);
        });
        
        test('all points in second half', () => {
            let walkwayProb = new WalkwayProblem1D([2,100], 20);
            let walkway = {'a': 0, 'b': 2};
            assert.equal(98, walkwayProb.maxDistanceSameHalf(walkway).d);
        });
        
        test('one point in first half, one point in second half', () => {
            let walkwayProb = new WalkwayProblem1D([1,2], 20);
            let walkway = {'a': 0, 'b': 2};
            assert.isNull(walkwayProb.maxDistanceSameHalf(walkway));
        });
    });

    suite('maxDistanceTypeIIandIII', () => {
        test('no points in region', () => {
            let walkwayProb = new WalkwayProblem1D([-100, 100], 20);
            let walkway = {'a': 0, 'b': 2};
            assert.isNull(walkwayProb.maxDistanceTypeIIandIII(walkway));
        });

        test('two points in region', () => {
            let walkwayProb = new WalkwayProblem1D([1,9], 20);
            let walkway = {'a': 0, 'b': 10};
            let expected = walkwayProb.travelTime(walkway, 0, 1);
            assert.deepEqual(expected,
                         walkwayProb.maxDistanceTypeIIandIII(walkway));
        });
        
        test('many points in region', () => {
            let walkwayProb = new WalkwayProblem1D([1,2,3,6,21,88,91,99,100], 20);
            let walkway = {'a': 0, 'b': 100};
            assert.deepEqual(
                // 4,5 => 38
                walkwayProb.maxDistanceTypeIIandIIIBruteForce(walkway),
                walkwayProb.maxDistanceTypeIIandIII(walkway));
        });
    });
});



suite('ConvexHull', () => {
    suite('antipodalVertices()', () => {
        test('triangle', () => {
            let triangle = [
                {'x':0,'y':0},
                {'x':2,'y':0},
                {'x':1,'y':1}
            ];
            let convexHull = new ConvexHull(triangle);

            assert.deepEqual([[0,2],[1,2],[1,0]], convexHull.antipodalVertices());
        });

        test('quadrilateral', () => {
            let quadrilateral = [
                {'x':0,'y':0},
                {'x':1,'y':0},
                {'x':0.75,'y':0.8},
                {'x':0.25,'y':1}
            ];
            let convexHull = new ConvexHull(quadrilateral);

            assert.deepEqual(
                [[0,3],[1,3],[1,0],[2,0]],
                convexHull.antipodalVertices());
        });

        test('parallel edges', () => {
            let quadrilateral = [
                {'x':0,'y':0},
                {'x':4,'y':0},
                {'x':3,'y':1},
                {'x':2,'y':1}
            ];

            let convexHull = new ConvexHull(quadrilateral);
            assert.deepEqual(
                [[0,2],[1,2],[0,3],[1,3],[1,0]],
                convexHull.antipodalVertices());
        });
    });
});

suite('Vector', () => {
    suite('angle()', () => {
        test('Quadrant I, Quadrant I', () => {
            let vec1 = new Vector(1,0);
            let vec2 = new Vector(1,1);
            assert.closeTo(Vector.angle(vec1, vec2), Math.PI/4, 0.1);
        });
        
        test('Quadrant I, Quadrant III', () => {
            let vec1 = new Vector(1,0);
            let vec2 = new Vector(-1,-1);
            assert.closeTo(Vector.angle(vec1, vec2), 5*Math.PI/4, 0.1);
        });

        test('Quadrant II, Quadrant III', () => {
            let vec1 = new Vector(-1,1);
            let vec2 = new Vector(-1,-1);
            assert.closeTo(Vector.angle(vec1, vec2), Math.PI/2, 0.1);
        });

        test('Quadrant III, Quadrant IV', () => {
            let vec1 = new Vector(-1,-1);
            let vec2 = new Vector(1,-1);
            assert.closeTo(Vector.angle(vec1, vec2), Math.PI/2, 0.1);
        });

        test('Quadrant IV, Quadrant I', () => {
            let vec1 = new Vector(1,-1);
            let vec2 = new Vector(1,1);
            assert.closeTo(Vector.angle(vec1, vec2), Math.PI/2, 0.1);
        });
        
    });
});

suite('center_and_highways', () => {
    suite('l1_dist', () => {
        test('simple_test', () => {
            var vec1 = new Vector(0, 0)
            var vec2 = new Vector(1, 1)
            assert.equal(2, vec1.l1_dist(vec2))
        });

        test('negative_values', () => {
            var vec1 = new Vector(0, 0)
            var vec2 = new Vector(-5, 3)
            assert.equal(8, vec1.l1_dist(vec2))
        });

        test('larger_values', () => {
            var vec1 = new Vector(2, 8)
            var vec2 = new Vector(-31, 14)
            assert.equal(39, vec1.l1_dist(vec2))
        });
    });

    suite('linf_dist', () => {
        test('simple_test', () => {
            var vec1 = new Vector(0, 0)
            var vec2 = new Vector(1, 1)
            assert.equal(1, vec1.linf_dist(vec2))
        });

        test('negative_values', () => {
            var vec1 = new Vector(0, 0)
            var vec2 = new Vector(-5, 3)
            assert.equal(5, vec1.linf_dist(vec2))
        });

        test('larger_values', () => {
            var vec1 = new Vector(2, 8)
            var vec2 = new Vector(-31, 14)
            assert.equal(33, vec1.linf_dist(vec2))
        });
    });
});


suite('2 dimensional walkway', () => {
    setup(() => {});

    suite('getFurthestPoints()', () => {
        test('With no walkway', () => {
            let points = [
                new Vector(1, 0),
                new Vector(1.5, 0),
                new Vector(2, 0),
                new Vector(2.1, 0)
            ];
            let walkway = {
                a: new Vector(1,0),
                b: new Vector(2, 0)
            }
            let walkwayProb = new WalkwayProblem2D(points, 1, 100, 100);
            let furthestPoints = walkwayProb
                .getFurthestPoints(walkway, 0.2);

            let expected = [
                {travelTime: 1, i: 0, j: 2},
                {travelTime: 1.1, i: 0, j: 3}
            ];
            assert.deepEqual(expected, furthestPoints);
        });
    });

    suite('optimalWalkwayLocation()', () => {
        test('Two points', () => {
            let points = [
                new Vector(10,10),
                new Vector(100,10)
            ];
            let walkwayProb = new WalkwayProblem2D(points, 2, 1000, 1000);
            let optimalWalkway = walkwayProb.optimalWalkwayLocation();

            assert.closeTo(points[0].x, optimalWalkway.a.x, 1);
            assert.closeTo(points[0].y, optimalWalkway.a.y, 1);
            assert.closeTo(points[1].x, optimalWalkway.b.x, 1);
            assert.closeTo(points[1].y, optimalWalkway.b.y, 1);
        });

        test('Four points', () => {
            let points = [
                new Vector(10,10),
                new Vector(10,20),
                new Vector(100, 10),
                new Vector(100, 20),
            ];

            let walkwayProb = new WalkwayProblem2D(points, 2, 1000, 1000);
            let optimalWalkway = walkwayProb.optimalWalkwayLocation();

            assert.closeTo(10, optimalWalkway.a.x, 3);
            assert.closeTo(15, optimalWalkway.a.y, 3);
            assert.closeTo(100, optimalWalkway.b.x, 3);
            assert.closeTo(15, optimalWalkway.b.y, 3);
        });
    });
});

suite('LP Solver', () => {
    setup(() => {});

    suite('solve()', () => {
        test('Test with complicated system of inequalities', () => {
            let objectiveFunction = [-1, 0, 0, 0];
            let lv = (8-(2/3))/2;
            let inequalities = [
                { lhs: [1, -1, -1, -lv], rhs: -2 },
                { lhs: [1, 1, 1, lv], rhs: 19 - lv },
                { lhs: [1, 0.5, 1, lv], rhs: 12.5 },
                { lhs: [1,1,-1,lv], rhs: 12-lv },
                { lhs: [1,-1,1,-lv], rhs: 3 },
                { lhs: [1,-0.5,-1,-lv], rhs: -1 },
                { lhs: [1,-0.5, 1, -lv], rhs: 3 },
                { lhs: [1, 0.5, -1, lv], rhs: 4.5 },
                { lhs: [0, 0, 0, 1], rhs: 0 },
                { lhs: [0, 0, 0, -1], rhs: -1 }
            ];
            let lp = new BoundedLinearProgram(inequalities, objectiveFunction, 1000);
            let solution = lp.solve();

            // Tested on https://online-optimizer.appspot.com
            // The same value was returned there (but with a different solution)
            assert.closeTo(-6.66666, lp.evaluateObjectiveFunction(solution), 0.1);

            for (inequality of inequalities) {
                assert(BoundedLinearProgram.testInequality(solution, inequality));
            }
        });
    });
});
